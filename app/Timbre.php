<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;
use Session;

class Timbre extends Model
{
    
	function __construct(){
        
		$this->user_id=Session::get("user_id");
		$this->user_name=Session::get("user_name");
		$this->user_email=Session::get("user_email"); 
        $this->user_type=Session::get("user_type");
        $this->user_org=Session::get("user_org");
		
	 }	
    public function getallUsers()
    {  	
         return DB::table('users')->orderBy("id","desc")->paginate(15);
       
    }

	public function getallResult()
    {  	
        //  return DB::table('personal_info')->orderBy("record_id","desc")->paginate(10);
        if($this->user_type=='2' || $this->user_type=='3'){
            return DB::table('personal_info')->where('organization','like','%'.$this->user_org.'%')->orderBy("record_id","desc")->paginate(10);
        }
         return DB::table('personal_info')->orderBy("record_id","desc")->paginate(10);
       
    }

    public function getallcopdResult()
    {  	
        // if($this->user_type=='2' || $this->user_type=='3'){result_from_copd
        //     return DB::table('resultfromtimbre')->where('Occupation','like','%'.$this->user_org.'%')->orderBy("Patient_ID","desc")->paginate(10);
        // }
         return DB::table('copd_results')->orderBy("ID","desc")->paginate(10);
       
    }
    public function getallcopdResultAll()
    {  	
         return DB::table('result_from_copd')->orderBy("record_id","desc")->get();
     }
     

    public function getallLeads()
    {  	
         return DB::table('personal_info')->orderBy("record_id","desc")->paginate(10);
       
    }

    public function getCopdallLeads()
    {  	
       return  DB::table('copd_personal_info')->orderBy("record_id","desc")->paginate(10);
       
    }

    public function getcopdleadsallLeads()
    {  	
         return DB::table('copd_personal_info')->orderBy("record_id","desc")->paginate(10);
       
    }
    

    public function getallLeads1()
    {  	
         return DB::table('personal_info')->get();
       
    }
     public function getallLeadsByParams($s,$i,$search,$org,$fromdate,$todate)
    {  	

       $start = Carbon::parse($fromdate);
       $end = Carbon::parse($todate);  
         $q= DB::table('personal_info')
         ->join("clinical_info_one","clinical_info_one.record_id",'=',"personal_info.record_id")
         ->join("clinical_info_two","clinical_info_two.record_id",'=',"personal_info.record_id");
         if( $search!=""){
            $q->where('personal_info.firstname', 'like', '%'.$search.'%');
         }
         if( $org!=""){
           $q->where('personal_info.organization',$org);
        }
        if($fromdate!="" && $todate!="" ){
           $q->whereBetween('personal_info.created_on',[$start,$end]);
       }
       if($fromdate!="" && $todate=="" ){
           $q->whereDate('personal_info.created_on',$start); 
       }
         return $q->get();
         exit;
         //->select($s)
       
    }

    public function insetResult($id)
    {  
         $q= DB::table('copd_personal_info')
         ->join("copd_clinical_info","copd_clinical_info.record_id",'=',"copd_personal_info.record_id");
         $q->where('copd_personal_info.record_id',$id);         
         $data=$q->get();
         $values = array(
            'Terget_Status'=>'0',
            'record_id'=>$data[0]->record_id,
            'Name'=>$data[0]->name,
            'gender'=>$data[0]->gender,
            'age'=>$data[0]->age,
            'current_location'=>$data[0]->current_location,
            'height_feet'=>$data[0]->height_feet,
            'height_inches'=>$data[0]->height_inches,
            'weight'=>$data[0]->weight,
            'organization'=>$data[0]->organization,
            'occupation'=>$data[0]->occupation,
            'smoking'=>$data[0]->smoking,
            'marital_status'=>$data[0]->marital_status,
            'lung_conditions'=>$data[0]->lung_conditions,
            'sob'=>$data[0]->sob,
            'sputum'=>$data[0]->sputum,
            'wheezing'=>$data[0]->wheezing,
            'chest_tightness'=>$data[0]->chest_tightness,
            'x200Hz_LEFT_SUM'=>null,
            'x200_500Hz_LEFT_SUM'=>null,
            'x500_1000Hz_LEFT_SUM'=>null,
            'x1000_1500Hz_LEFT_SUM'=>null,
            'x1500_2000Hz_LEFT_SUM'=>null,
            'x2000_2500Hz_LEFT_SUM'=>null,
            'x2500_3000Hz_LEFT_SUM'=>null,
            'x3000_3500Hz_LEFT_SUM'=>null,
            'x3500_4000Hz_LEFT_SUM'=>null,
            'x4000_4500Hz_LEFT_SUM'=>null,
            'x4500_5000Hz_LEFT_SUM'=>null,
            'x200Hz_LEFT_MEAN'=>null,
            'x200_500Hz_LEFT_MEAN'=>null,
            'x500_1000Hz_LEFT_MEAN'=>null,
            'x1000_1500Hz_LEFT_MEAN'=>null,
            'x1500_2000Hz_LEFT_MEAN'=>null,
            'x2000_2500Hz_LEFT_MEAN'=>null,
            'x2500_3000Hz_LEFT_MEAN'=>null,
            'x3000_3500Hz_LEFT_MEAN'=>null,
            'x3500_4000Hz_LEFT_MEAN'=>null,
            'x4000_4500Hz_LEFT_MEAN'=>null,
            'x4500_5000Hz_LEFT_MEAN'=>null,
            'x200Hz_LEFT_SD'=>null,
            'x200_500Hz_LEFT_SD'=>null,
            'x500_1000Hz_LEFT_SD'=>null,
            'x1000_1500Hz_LEFT_SD'=>null,
            'x1500_2000Hz_LEFT_SD'=>null,
            'x2000_2500Hz_LEFT_SD'=>null,
            'x2500_3000Hz_LEFT_SD'=>null,
            'x3000_3500Hz_LEFT_SD'=>null,
            'x3500_4000Hz_LEFT_SD'=>null,
            'x4000_4500Hz_LEFT_SD'=>null,
            'x4500_5000Hz_LEFT_SD'=>null,
            'x200Hz_LEFT_VARIANCE'=>null,
            'x200_500Hz_LEFT_VARIANCE'=>null,
            'x500_1000Hz_LEFT_VARIANCE'=>null,
            'x1000_1500Hz_LEFT_VARIANCE'=>null,
            'x1500_2000Hz_LEFT_VARIANCE'=>null,
            'x2000_2500Hz_LEFT_VARIANCE'=>null,
            'x2500_3000Hz_LEFT_VARIANCE'=>null,
            'x3000_3500Hz_LEFT_VARIANCE'=>null,
            'x3500_4000Hz_LEFT_VARIANCE'=>null,
            'x4000_4500Hz_LEFT_VARIANCE'=>null,
            'x4500_5000Hz_LEFT_VARIANCE'=>null,
            'x200Hz_LEFT_COEFF_LEFT_VARIANCE'=>null,
            'x200_500Hz_LEFT_COEFF_LEFT_VARIANCE'=>null,
            'x500_1000Hz_LEFT_COEFF_LEFT_VARIANCE'=>null,
            'x1000_1500Hz_LEFT_COEFF_LEFT_VARIANCE'=>null,
            'x1500_2000Hz_LEFT_COEFF_LEFT_VARIANCE'=>null,
            'x2000_2500Hz_LEFT_COEFF_LEFT_VARIANCE'=>null,
            'x2500_3000Hz_LEFT_COEFF_LEFT_VARIANCE'=>null,
            'x3000_3500Hz_LEFT_COEFF_LEFT_VARIANCE'=>null,
            'x3500_4000Hz_LEFT_COEFF_LEFT_VARIANCE'=>null,
            'x4000_4500Hz_LEFT_COEFF_LEFT_VARIANCE'=>null,
            'x4500_5000Hz_LEFT_COEFF_LEFT_VARIANCE'=>null,
            'x200Hz_LEFT_TOP10_LEFT_AMPLITUDE'=>null,
            'x200_500Hz_LEFT_TOP10_LEFT_AMPLITUDE'=>null,
            'x500_1000Hz_LEFT_TOP10_LEFT_AMPLITUDE'=>null,
            'x1000_1500Hz_LEFT_TOP10_LEFT_AMPLITUDE'=>null,
            'x1500_2000Hz_LEFT_TOP10_LEFT_AMPLITUDE'=>null,
            'x2000_2500Hz_LEFT_TOP10_LEFT_AMPLITUDE'=>null,
            'x2500_3000Hz_LEFT_TOP10_LEFT_AMPLITUDE'=>null,
            'x3000_3500Hz_LEFT_TOP10_LEFT_AMPLITUDE'=>null,
            'x3500_4000Hz_LEFT_TOP10_LEFT_AMPLITUDE'=>null,
            'x4000_4500Hz_LEFT_TOP10_LEFT_AMPLITUDE'=>null,
            'x4500_5000Hz_LEFT_TOP10_LEFT_AMPLITUDE'=>null,
            'x200Hz_LEFT_SPECTRAL_LEFT_CENTROID'=>null,
            'x200_500Hz_LEFT_SPECTRAL_LEFT_CENTROID'=>null,
            'x500_1000Hz_LEFT_SPECTRAL_LEFT_CENTROID'=>null,
            'x1000_1500Hz_LEFT_SPECTRAL_LEFT_CENTROID'=>null,
            'x1500_2000Hz_LEFT_SPECTRAL_LEFT_CENTROID'=>null,
            'x2000_2500Hz_LEFT_SPECTRAL_LEFT_CENTROID'=>null,
            'x2500_3000Hz_LEFT_SPECTRAL_LEFT_CENTROID'=>null,
            'x3000_3500Hz_LEFT_SPECTRAL_LEFT_CENTROID'=>null,
            'x3500_4000Hz_LEFT_SPECTRAL_LEFT_CENTROID'=>null,
            'x4000_4500Hz_LEFT_SPECTRAL_LEFT_CENTROID'=>null,
            'x4500_5000Hz_LEFT_SPECTRAL_LEFT_CENTROID'=>null,
            'x200Hz_LEFT_SPECTRAL_LEFT_FLATNESS'=>null,
            'x200_500Hz_LEFT_SPECTRAL_LEFT_FLATNESS'=>null,
            'x500_1000Hz_LEFT_SPECTRAL_LEFT_FLATNESS'=>null,
            'x1000_1500Hz_LEFT_SPECTRAL_LEFT_FLATNESS'=>null,
            'x1500_2000Hz_LEFT_SPECTRAL_LEFT_FLATNESS'=>null,
            'x2000_2500Hz_LEFT_SPECTRAL_LEFT_FLATNESS'=>null,
            'x2500_3000Hz_LEFT_SPECTRAL_LEFT_FLATNESS'=>null,
            'x3000_3500Hz_LEFT_SPECTRAL_LEFT_FLATNESS'=>null,
            'x3500_4000Hz_LEFT_SPECTRAL_LEFT_FLATNESS'=>null,
            'x4000_4500Hz_LEFT_SPECTRAL_LEFT_FLATNESS'=>null,
            'x4500_5000Hz_LEFT_SPECTRAL_LEFT_FLATNESS'=>null,
            'x200Hz_LEFT_SPECTRAL_LEFT_SKEWNESS'=>null,
            'x200_500Hz_LEFT_SPECTRAL_LEFT_SKEWNESS'=>null,
            'x500_1000Hz_LEFT_SPECTRAL_LEFT_SKEWNESS'=>null,
            'x1000_1500Hz_LEFT_SPECTRAL_LEFT_SKEWNESS'=>null,
            'x1500_2000Hz_LEFT_SPECTRAL_LEFT_SKEWNESS'=>null,
            'x2000_2500Hz_LEFT_SPECTRAL_LEFT_SKEWNESS'=>null,
            'x2500_3000Hz_LEFT_SPECTRAL_LEFT_SKEWNESS'=>null,
            'x3000_3500Hz_LEFT_SPECTRAL_LEFT_SKEWNESS'=>null,
            'x3500_4000Hz_LEFT_SPECTRAL_LEFT_SKEWNESS'=>null,
            'x4000_4500Hz_LEFT_SPECTRAL_LEFT_SKEWNESS'=>null,
            'x4500_5000Hz_LEFT_SPECTRAL_LEFT_SKEWNESS'=>null,
            'x200Hz_LEFT_KURTOSIS'=>null,
            'x200_500Hz_LEFT_KURTOSIS'=>null,
            'x500_1000Hz_LEFT_KURTOSIS'=>null,
            'x1000_1500Hz_LEFT_KURTOSIS'=>null,
            'x1500_2000Hz_LEFT_KURTOSIS'=>null,
            'x2000_2500Hz_LEFT_KURTOSIS'=>null,
            'x2500_3000Hz_LEFT_KURTOSIS'=>null,
            'x3000_3500Hz_LEFT_KURTOSIS'=>null,
            'x3500_4000Hz_LEFT_KURTOSIS'=>null,
            'x4000_4500Hz_LEFT_KURTOSIS'=>null,
            'x4500_5000Hz_LEFT_KURTOSIS'=>null,
            'x200Hz_LEFT_MFCC'=>null,
            'x200_500Hz_LEFT_MFCC'=>null,
            'x500_1000Hz_LEFT_MFCC'=>null,
            'x1000_1500Hz_LEFT_MFCC'=>null,
            'x1500_2000Hz_LEFT_MFCC'=>null,
            'x2000_2500Hz_LEFT_MFCC'=>null,
            'x2500_3000Hz_LEFT_MFCC'=>null,
            'x3000_3500Hz_LEFT_MFCC'=>null,
            'x3500_4000Hz_LEFT_MFCC'=>null,
            'x4000_4500Hz_LEFT_MFCC'=>null,
            'x4500_5000Hz_LEFT_MFCC'=>null,
            'x200Hz_LEFT_Energy'=>null,
            'x200_500Hz_LEFT_Energy'=>null,
            'x500_1000Hz_LEFT_Energy'=>null,
            'x1000_1500Hz_LEFT_Energy'=>null,
            'x1500_2000Hz_LEFT_Energy'=>null,
            'x2000_2500Hz_LEFT_Energy'=>null,
            'x2500_3000Hz_LEFT_Energy'=>null,
            'x3000_3500Hz_LEFT_Energy'=>null,
            'x3500_4000Hz_LEFT_Energy'=>null,
            'x4000_4500Hz_LEFT_Energy'=>null,
            'x4500_5000Hz_LEFT_Energy'=>null);
         return DB::table('result_from_copd')->insert($values); 
}

    
    public function getallcopdLeadsByParams($s,$i,$search,$org,$fromdate,$todate)
    {  	
       
       $start = Carbon::parse($fromdate);
       $end = Carbon::parse($todate);  
         $q= DB::table('copd_personal_info')
         ->join("copd_clinical_info","copd_clinical_info.record_id",'=',"copd_personal_info.record_id");
         
         if( $search!=""){
            $q->where('copd_personal_info.name', 'like', '%'.$search.'%');
         }
         if( $org!=""){
           $q->where('copd_personal_info.organization',$org);
        }
        if($fromdate!="" && $todate!="" ){
           $q->whereBetween('copd_personal_info.created_on',[$start,$end]);
       }
       if($fromdate!="" && $todate=="" ){
           $q->whereDate('copd_personal_info.created_on',$start); 
       }
       
         return $q->get();
         exit;
         //->select($s)
       
    }
    
    public function getallMedia($s,$i,$search,$org,$fromdate,$todate)
    {  	

       $start = Carbon::parse($fromdate);
       $end = Carbon::parse($todate);  
         $q= DB::table('personal_info');
         if( $search!=""){
            $q->where('personal_info.firstname', 'like', '%'.$search.'%');
         }
         if( $org!=""){
           $q->where('personal_info.organization',$org);
        }
        if($fromdate!="" && $todate!="" ){
           $q->whereBetween('personal_info.created_on',[$start,$end]);
       }
       if($fromdate!="" && $todate=="" ){
           $q->whereDate('personal_info.created_on',$start); 
       }
         return $q->get();
         exit;
         //->select($s)
       
    }

    public function getAllParams($id){
       return DB::table('personal_info')
        ->join("clinical_info_one","clinical_info_one.record_id",'=',"personal_info.record_id")
        ->join("clinical_info_two","clinical_info_two.record_id",'=',"personal_info.record_id")
        ->where('personal_info.record_id',$id)
        ->get();
    }
    public function getcopdAllParams($id){
        return DB::table('copd_personal_info')
        ->join("copd_clinical_info","copd_clinical_info.record_id",'=',"copd_personal_info.record_id")
        ->where('copd_personal_info.record_id',$id)
        ->get();
     }

     public function getcopdAllParamsmodel($id){
        return DB::table('copd_personal_info')
        ->join("copd_clinical_info","copd_clinical_info.record_id",'=',"copd_personal_info.record_id")
        ->where('copd_personal_info.record_id',$id)
        ->get();

        
     }

    
    public function getAllParamsOfResult($id){
        return DB::table('resultfromtimbre')->where("Patient_ID",$id)->get();
     }
    

    
    public function modelDeleteUser($record_id){
       // echo $record_id;exit(); 
        DB::table('personal_info')->where('record_id', $record_id)->delete();
        
    }
    public function deleteCopdLead($record_id){
        // echo $record_id;exit(); 
         DB::table('copd_personal_info')->where('record_id', $record_id)->delete();
         DB::table('copd_clinical_info')->where('record_id', $record_id)->delete();
         
     }
    public function loginmodel($data)
    {
       
       if($data['username']!='' and $data['password']!='')
       {           
          $email=$data['username'];
          $pwd=$data['password']; 
          $validate= DB::table('users')->where('email',$email)->where('password',$pwd)->get();              
          if(sizeOf($validate)>0){
              foreach ($validate as $value) {   
                    Session::put("user_id",$value->id);
                    Session::put("user_name",$value->name);                   
                    Session::put("user_email",$value->email);
                    Session::put("user_img",$value->mobile);
                    Session::put("user_type",$value->role); 
                    Session::put("user_org",$value->org); 
                                   
                }
              return 1;
          }else{
              return 0;
          }
       }else{
           return 0;     
       }
    }

    public function checkLogin($data)
    {
       
       if($data['username']!='' and $data['password']!='')
       {           
          $email=$data['username'];
          $pwd=$data['password']; 
          $validate= DB::table('users')->where('email',$email)->where('password',$pwd)->get();              
          if(sizeOf($validate)>0){
            return array("status"=>true,"data"=> $validate);
          }else{
              return array("status"=>false) ;
          }
       }else{
         return array("status"=>false) ;     
       }
    }
     public function addUserdata($data)
    {
          $name=$data['usr'];
          $pwd=$data['pwd'];
          $email=$data['email'];
          $mobile=$data['mobile']; 
          $role=$data['role'];
          $org=$data['org'];
          $values = array('name' => $name,'email' => $email,'password' => $pwd,'mobile' => $mobile, 'role' => $role,'org'=>$org);
          DB::table('users')->insert($values); 
    }

    public function modelDeleteUserRole($record_id){
        DB::table('users')->where('id', $record_id)->delete();
    }

    public function filter($data){
       $fromDate=$data['fromDate'];
       $toDate=$data['toDate'];
       $searchdata=$data['src_string'];
       $org=$data['org'];
       $src_id_string=$data['src_id_string'];
             $start = Carbon::parse($fromDate);
            $end = Carbon::parse($toDate);  
      $q=DB::table('personal_info');
      if( $searchdata!=""){
         $q->where('firstname', 'like', '%'.$searchdata.'%');
      }
      if( $org!=""){
        $q->where('organization',$org);
     }
     if( $src_id_string!=""){
        $q->where('record_id',$src_id_string);
     }
     if($fromDate!="" && $toDate!="" ){
        $q->whereBetween('created_on',[$start,$end]); //->get();
    }
    if($fromDate!="" && $toDate=="" ){
        $q->whereDate('created_on',$start); //->get(); 
    }
   return  $q->orderBy("record_id","desc")->get();
        

       


        
     
        // if($data['filter_type']=='date'){
        //     $fromDate=$data['fromDate'];
        //     $toDate=$data['toDate'];
        //     $start = Carbon::parse($fromDate);
        //     $end = Carbon::parse($toDate);           
        //     if($fromDate!="" && $toDate!="" ){
        //         $d=DB::table('personal_info')->whereBetween('created_on',[$start,$end])->get(); //->paginate(10);
        //     }
        //     if($fromDate!="" && $toDate=="" ){
        //         $d=DB::table('personal_info')->whereDate('created_on',$start)->get(); //->paginate(10);
        //     }
        //     return $d;
        // }
        // if($data['filter_type']=='search'){
           
        //     $searchdata=$data['src_string'];
        //     $searchtype=$data['type'];
            
        //     if($searchtype=="org"){
        //         $users = DB::table('personal_info')->where('organization', 'like', '%'.$searchdata.'%')->orderBy("record_id","desc")->get(); //->paginate(10);
        //     }
        //     if($searchtype=="name"){
        //         $users = DB::table('personal_info')->where('firstname', 'like', '%'.$searchdata.'%')->orderBy("record_id","desc")->get(); //->paginate(10);
        //     }   
            
        //     //$results = DB::select( DB::raw("SELECT * FROM personal_info1 WHERE (firstname like '%$searchdata' OR organization like '%$searchdata%') order by record_id Desc"))->paginate(10);
        //     return $users;

        // }
        // if($data['filter_type']=='cat'){
        //     $searchdata=$data['org'];
        //     $users = DB::table('personal_info')
        //     ->where('organization',$searchdata)
        //     ->orderBy("record_id","desc")->get(); //->paginate(10);
        //     return $users;
        // }
        
        // return DB::table('personal_info')->orderBy("record_id","desc")->paginate(10);
    }


    public function copdfilter($data){
        $fromDate=$data['fromDate'];
        $toDate=$data['toDate'];
        $searchdata=$data['src_string'];
        $org=$data['org'];
        $src_id_string=$data['src_id_string'];
              $start = Carbon::parse($fromDate);
             $end = Carbon::parse($toDate);  
       $q=DB::table('copd_personal_info');
       if( $searchdata!=""){
          $q->where('name', 'like', '%'.$searchdata.'%');
       }
       if( $org!=""){
         $q->where('organization',$org);
      }
      if( $src_id_string!=""){
         $q->where('record_id',$src_id_string);
      }
      if($fromDate!="" && $toDate!="" ){
         $q->whereBetween('created_on',[$start,$end]); //->get();
     }
     if($fromDate!="" && $toDate=="" ){
         $q->whereDate('created_on',$start); //->get(); 
     }
    return  $q->orderBy("record_id","desc")->get();
         
     }

    public function getOrganisations(){
          $results = DB::select( DB::raw("SELECT organization from personal_info group by organization")); 
          return $results;
    }

    public function gecopdtOrganisations(){
        $results = DB::select( DB::raw("SELECT organization from copd_personal_info group by organization")); 
        return $results;
  }

//     public function getOrganisations(){
//         $results = DB::select( DB::raw("SELECT organization from personal_info group by organization")); 
//         return $results;
//   }

    public function resultfilter($data){



        
        $searchdata=isset($data['src_string']) ? $data['src_string'] : "";
		$org=isset($data['org']) ? $data['org'] : "";
		$pid=isset($data['pid']) ? $data['pid'] : "";
		$status=isset($data['status']) ? $data['status'] : "";
      $q=DB::table('resultfromtimbre');
      if( $searchdata!=""){
         $q->where('Name', 'like', '%'.$searchdata.'%');
      }
      if( $org!=""){
        $q->where('Occupation',$org);
     }
     if($status!=""){
        $q->where('Predicted_Target_status',$status);
    }
    if($pid!=""){
        $q->where('Patient_ID',$pid);
    }
   return  $q->orderBy("Patient_ID","desc")->get();     

    }
                                                                            

  



    // Master Data getallOrganations

    public function getallOrganations(){
        return DB::table('organization')->get();
    }


    public function addOrganizationmodel($data){
        //print_r($data);exit();
        $org_name=$data['org_name'];
        $org_dec=$data['org_dc'];
        
        $values = array('org_name' => $org_name,'org_description' => $org_dec);
        DB::table('organization')->insert($values); 
    }

    public function modeldeleteOrganization($id){
        DB::table('organization')->where('id', $id)->delete(); 
    }
}
