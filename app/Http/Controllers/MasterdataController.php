<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Timbre;
use Session;
use View;
use Excel;

class MasterdataController extends Controller
{
    function __construct(){
		$this->user_id=Session::get("user_id");
		$user_name=Session::get("user_name");
		$user_email=Session::get("user_email"); 
        $user_type=Session::get("user_type");
        $user_org=Session::get("user_org");
		$this->obj=new Timbre();
		// if(!$this->user_id){
		// 	$error=array('message' => "Session is Expired" );
		// 	return view("login",compact('error'));
		// }
 		if($user_name){
				View::share("uname",$user_name);
				View::share("user_email",$user_email);  
                 View::share("user_type",$user_type); 
                 View::share("user_type",$user_type);   
                 View::share("user_org",$user_org);             
	 }

    

     }
     
    public function Accupation(){
    	return view('accupation');
    }

    public function Supplyment(){
    	return view('supplyment');
    }

    public function Othercough(){
    	return view('othercough');
    }

    public function Addorganation(){
    	return view('addorganation');
    }

    public function addOrganizationcontroll(Request $request){
        if(!$this->user_id){
			Session::flash('message', 'Session Expired!');
			return redirect("login");
        }
        //echo "prasad"; exit();

		$data=$request->all();     
        $res=$this->obj->addOrganizationmodel($data);
		return redirect("organation-list");
    }

    public function OrganationList(){
        if(!$this->user_id){
            Session::flash('message', 'Session Expired!');
            return redirect("login");
        }
        $dbresult=$this->obj->getallOrganations();
    	return view('organation-list',compact('dbresult'));
    }

    public function deleteOrganization($id){
        if(!$this->user_id){
			Session::flash('message', 'Session Expired!');
			return redirect("login");
        }
		$deleteOrg=$this->obj->modeldeleteOrganization($id);
	    return redirect('/organation-list');
    }


    
    
}
