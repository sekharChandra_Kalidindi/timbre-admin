<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Timbre;
use Session;
use View;
use Excel;
use ZipArchive;
use Carbon\Carbon;
use DB;



class HomeController extends Controller{


	function __construct(){
		$this->user_id=Session::get("user_id");
		$user_name=Session::get("user_name");
		$user_email=Session::get("user_email"); 
		$user_type=Session::get("user_type");
		$user_org=Session::get("user_org");
		
		$this->obj=new Timbre();
		// if(!$this->user_id){
		// 	$error=array('message' => "Session is Expired" );
		// 	return view("login",compact('error'));
		// }
 		if($user_name){
				View::share("uname",$user_name);
				View::share("user_email",$user_email);  
				 View::share("user_type",$user_type);
				 View::share("user_org",$user_org);             
	 }

    

	 }	
	 
	public function login()
    {	
    	return view('login');
	}
	public function logout(){
		Session::flush();
		return redirect('login');
}

	public function logincon(Request $request)
    {
	
		   $data=$request->all();     
        $res=$this->obj->loginmodel($data);
       if($res==1){
       	return redirect("dashboard");
       }else{
          $errorArray=array();
          if($res==0){
						Session::put('message', 'User is not found!');
            $error=array('message' => "User is not found" );
            $errorArray=$error;
          }else if($res==0){
						Session::put('message', 'Password missmatch!');
            $error=array('message' => "Password missmatch" );
            $errorArray=$error;
          }else{
						Session::put('message', 'something went wrong!');
            $error=array('message' => "something went wrong" );
            $errorArray=$error;
          }
           return redirect("login");
       }
	}
	function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function index()
    {	
		if(!$this->user_id){
			Session::flash('message', 'Session Expired!');
			return redirect("login");
		}
    	return view('index');
	}
	public function downloadMedia(Request $request){

		$search=$request->input('search');
		$org=$request->input('org');
		$fromdate=$request->input('fromdate');
		$todate=$request->input('todate');
		$type = $request->input('type');
		$public_dir=public_path();
		$items = $this->obj->getallMedia('','',$search,$org,$fromdate,$todate);	
		$bname="-images";
		$fnameKey="prescription_img";
		$bpath="/var/www/html/timbre-leads/api/images/";
		if($type=='Audio'){
			$bpath="/var/www/html/timbre-leads/api/uploads/";
			$bname="-audio";
			$fnameKey="audio_path";
		}
		$now = Carbon::now();
		$createdAt = Carbon::parse($now)->format('y-m-d');;
		$createdAt=$createdAt.$bname.'-'.$this->generateRandomString(5);
		$zipFileName = $createdAt.'.zip';
        $zip = new ZipArchive();
        if ($zip->open($public_dir . '/uploads/' . $zipFileName, ZipArchive::CREATE) === TRUE) {    
			// $path='/var/www/html/timbre-admin/public/images/logo.png';
			// $path1='/var/www/html/timbre-admin/public/images/avatar.png';
			foreach($items as $i){
				if($i->{$fnameKey}!="null"){
					if(file_exists($bpath.$i->{$fnameKey})){
					  $zip->addFile($bpath.$i->{$fnameKey},$i->{$fnameKey}); 
					}
					//echo $bpath.$i->audio_path.'<br/>';
				}
			}
			//$zip->addFile($path,Carbon::now().'-Screenshot (21).png');      
			//$zip->addFile($path1,Carbon::now().'-Screenshot (22).png');          
             $zip->close();
        }else{
			echo "not done";
		}
		// $z= \Zipper::make(public_path($createdAt.'-media-files.zip'));
		// foreach($items as $i){
		// 	$m= $i->audio_path;
		// 	$m=$i->prescription_img;
		// 	if($m!='' && $m!='null' && $m!=null){
		// 		$path='D:/xampp/htdocs/blog/app/New folder/api/images/Screenshot (21).png';
		// 		if(file_exists($path)){
			
		// 			$z->add($path,Carbon::now().'-Screenshot (21).png');
		// 		}	
		// 	}
		// }
		// $z->close();
		// return response()->download(public_path($createdAt.'-media-files.zip'));
		return response()->download($public_dir . '/uploads/' . $zipFileName);
	}

	public function addUser()
    {	
		
		$orgs=$this->obj->getallOrganations();
    	return view('add-user',compact('orgs'));
	}

    public function leads()
    {
			if(!$this->user_id){
				Session::flash('message', 'Session Expired!');
				return redirect("login");
			}
		$columns=array(array("accessKey"=>'record_id',"dispalayName"=>'Patient ID'),
			array("accessKey"=>'firstname',"dispalayName"=>'User Name'),
	    array("accessKey"=>'organization',"dispalayName"=>'Organization'));
	    $dbresult=$this->obj->getallLeads();
        return view('leads',compact('dbresult'));
	}
	public function copdleads()
    {
			if(!$this->user_id){
				Session::flash('message', 'Session Expired!');
				return redirect("login");
			}
			
	    $dbresult=$this->obj->getcopdleadsallLeads();
        return view('copd-leads',compact('dbresult'));
	}
	public function addResult()
    {
			if(!$this->user_id){
				Session::flash('message', 'Session Expired!');
				return redirect("login");
			}
        return view('add-result');
	}
	public function downloadLeads(Request $request){
		if(!$this->user_id){
			Session::flash('message', 'Session Expired!');
			return redirect("login");
		}
	//	`&search=${namesearch}&org=${org}&fromdate=${fromdate}&todate=${todate}`;
	$search=$request->input('search');
	$org=$request->input('org');
	$fromdate=$request->input('fromdate');
	$todate=$request->input('todate');

	   $type = $request->input('type');
	   if($type=='single'){
			$section = $request->input('selection');
			$s=explode(',',$section);
			$id = $request->input('selection');
			$i=explode(',',$id);
		   $items = $this->obj->getallLeadsByParams($s,$i,$search,$org,$fromdate,$todate);
		    $h = $s;
        $customer_array[] = $s;
		foreach($items as $customer)
		{
			$associativeArray = array();
			foreach($h as $k)
			{
			$associativeArray[$k]=$customer->{$k};
			}
			$customer_array[]=$associativeArray;
		// $customer_array[] = array(
		// 'Customer Name'  => $customer->firstname,
		// 'Address'   => $customer->lastname,
		// 'City'    => $customer->age,
		// 'Postal Code'  => $customer->gender,
		// 'Country'   => $customer->record_id
		// );
		}
       Excel::create('items', function($excel) use($customer_array) {
          $excel->sheet('ExportFile', function($sheet) use($customer_array) {
              $sheet->fromArray($customer_array);
          });
      })->export('xls');
	   }
		
	}

	public function user()
    {   	
			if(!$this->user_id){
				Session::flash('message', 'Session Expired!');
				return redirect("login");
			}
		$columns=array(array("accessKey"=>'id',"dispalayName"=>'User id'),
		array("accessKey"=>'name',"dispalayName"=>'User Name'),
				array("accessKey"=>'username',"dispalayName"=>'User Id'),
			array("accessKey"=>'created_on',"dispalayName"=>'Created On'));
	    	$dbresult=$this->obj->getallUsers();
	    //	print_r($dbresult);exit();
        return view('users' ,compact('dbresult','columns'));
	}

	 public function downloadData()
    {
			if(!$this->user_id){
				Session::flash('message', 'Session Expired!');
				return redirect("login");
			}
			$fromDate='';
			$toDate='';
			$searchdata='';
			$searchid='';
			$org='';
		  $dbresult=$this->obj->getallLeads();
		  $dbresult2=$this->obj->getOrganisations();
        return view('downloadData',compact('dbresult','dbresult2','fromDate','toDate','searchdata','org','searchid'));
	} 


	

	public function downloadcopdData(Request $request)
    { 
		
		if(!$this->user_id){
			Session::flash('message', 'Session Expired!');
			return redirect("login");
		}
	//	`&search=${namesearch}&org=${org}&fromdate=${fromdate}&todate=${todate}`;
		$search=$request->input('search');
		$org=$request->input('org');
		$fromdate=$request->input('fromdate');
		$todate=$request->input('todate');

	   $type = $request->input('type');
	   if($type=='single'){
			$section = $request->input('selection');
			$s=explode(',',$section);
			$id = $request->input('selection');
			$i=explode(',',$id);
		   $items = $this->obj->getallcopdLeadsByParams($s,$i,$search,$org,$fromdate,$todate);
		  print_r($items);
			$h = $s;
			
        $customer_array[] = $s;
		foreach($items as $customer)
		{
			$associativeArray = array();
			foreach($h as $k)
			{
			$associativeArray[$k]=$customer->{$k};

			
			}
			$customer_array[]=$associativeArray;
		
		}
		
      return Excel::create('items', function($excel) use($customer_array) {
		  
          $excel->sheet('ExportFile', function($sheet) use($customer_array) {
			   
			 $ss = $sheet->fromArray($customer_array);
			 
			
          });
      })->export('xls');
	   }
		} 


	public function downloadDatacopd()
    {
			if(!$this->user_id){
				Session::flash('message', 'Session Expired!');
				return redirect("login");
			}
			$fromDate='';
			$toDate='';
			$searchdata='';
			$searchid='';
			$org='';
		  $dbresult=$this->obj->getCopdallLeads();
		  //print_r($dbresult);exit();
		   $dbresult2=$this->obj->getOrganisations();
        return view('downloadDatacopd',compact('dbresult','dbresult2','fromDate','toDate','searchdata','org','searchid'));
	} 

	public function result()
    {
			if(!$this->user_id){
				Session::flash('message', 'Session Expired!');
				return redirect("login");
			}
    	$columns=array(array("accessKey"=>'id',"dispalayName"=>'id'),
    			array("accessKey"=>'patentid',"dispalayName"=>'Patients Id'),
	    		array("accessKey"=>'name',"dispalayName"=>'User Name'),
	    		array("accessKey"=>'timbre_result',"view_type"=>'ancor', "rout"=>'reports?id=2320',"dispalayName"=>'Result from Timbre'),);
		
				$fromDate='';
				$pid='';
				$searchdata='';
				$searchid='';
				$org='';
				$status='';
			   $dbresult2=$this->obj->getOrganisations();
				$dbresult=$this->obj->getallResult();
	    	// print_r($dbresult);exit();
        return view('result' ,compact('dbresult','dbresult2','status','pid','searchdata','org','searchid'));
	}


	public function copdresult()
    {
		
			if(!$this->user_id){
				Session::flash('message', 'Session Expired!');
				return redirect("login");
			}
    	$columns=array(array("accessKey"=>'id',"dispalayName"=>'id'),
    			array("accessKey"=>'patentid',"dispalayName"=>'Patients Id'),
	    		array("accessKey"=>'name',"dispalayName"=>'User Name'),
	    		array("accessKey"=>'timbre_result',"view_type"=>'ancor', "rout"=>'reports?id=2320',"dispalayName"=>'Result from Timbre'),);
		
				$fromDate='';
				$pid='';
				$searchdata='';
				$searchid='';
				$org='';
				$status='';
			   $dbresult2=$this->obj->gecopdtOrganisations();
				$dbresult=$this->obj->getallcopdResult();
	    	// print_r($dbresult);exit();
        return view('copdresult' ,compact('dbresult','dbresult2','status','pid','searchdata','org','searchid'));
	}

	public function copdreporatsMobile(){
		return view('copd-reporats-mobile');	
	}

	public function insetResult($record_id){
		$dbresult2=$this->obj->insetResult($record_id);

		return response()->json(array('status'=>$dbresult2)) ;	
	}

	public function getAllResults()
    {
		$dbresult=$this->obj->getallcopdResultALL();
		return response()->json(array("status"=>true,"data"=>$dbresult));
	}
	public function checkLogin(Request $request)
    {
		$data=$request->all();     
		$dbresult=$this->obj->checkLogin($data);
		return response()->json($dbresult);
	}

	public function reporats()
    {
			if(!$this->user_id){
				Session::flash('message', 'Session Expired!');
				return redirect("login");
			}
    	
        return view('reporats');
	}
	public function copdreporats()
    {
			if(!$this->user_id){
				Session::flash('message', 'Session Expired!');
				return redirect("login");
			}
    	
        return view('copd-reporats');
	}


	public function deleteUser($record_id){
		if(!$this->user_id){
			Session::flash('message', 'Session Expired!');
			return redirect("login");
		}
		$deleteResult=$this->obj->modelDeleteUser($record_id);
	    return redirect('/leads');
	}
	public function deleteCopdLead($record_id){
		if(!$this->user_id){
			Session::flash('message', 'Session Expired!');
			return redirect("login");
		}
		$deleteResult=$this->obj->deleteCopdLead($record_id);
	    return redirect('/copd-leads');
	}

	

	public function deleteUserRoles($record_id){
		if(!$this->user_id){
			Session::flash('message', 'Session Expired!');
			return redirect("login");
		}
		$deleteResult1=$this->obj->modelDeleteUserRole($record_id);
	    return redirect('/allusers');
	}



	public function addUserdata(Request $request){

		if(!$this->user_id){
			Session::flash('message', 'Session Expired!');
			return redirect("login");
		}

		$data=$request->all();     
        $res=$this->obj->addUserdata($data);
		return redirect("allusers");
	}
	public function filter(Request $request)
	{
		 $data=$request->all(); 
		 $fromDate=$data['fromDate'];
		 $toDate=$data['toDate'];
		 $searchdata=$data['src_string'];
		 $searchid=$data['src_id_string'];
		 $org=$data['org'];
		 $dbresult=$this->obj->filter($data);
		 $dbresult2=$this->obj->getOrganisations();
		 return view('downloadData',compact('dbresult','dbresult2','fromDate','toDate','searchdata','org','searchid'));
	}


	public function copdfilter(Request $request)
	{
		 $data=$request->all(); 
		 $fromDate=$data['fromDate'];
		 $toDate=$data['toDate'];
		 $searchdata=$data['src_string'];
		 $searchid=$data['src_id_string'];
		 $org=$data['org'];
		 $dbresult=$this->obj->copdfilter($data);
		 $dbresult2=$this->obj->getOrganisations();
		 return view('downloadDatacopd',compact('dbresult','dbresult2','fromDate','toDate','searchdata','org','searchid'));
	}

	public function resultfilter(Request $request){
		$data = $request->all();
		$searchdata=isset($data['src_string']) ? $data['src_string'] : "";
		$searchid=isset($data['src_id_string']) ? $data['src_id_string'] : "";
		$org=isset($data['org']) ? $data['org'] : "";
		$pid=isset($data['pid']) ? $data['pid'] : "";
		$status=isset($data['status']) ? $data['status'] : "";
		$dbresult = $this->obj->resultfilter($data);
		$dbresult2=$this->obj->getOrganisations();
		return view('result',compact('dbresult','dbresult2','pid','status','searchdata','org','searchid'));
	}

	public function getAllParams($record_id){
		$dbresult = $this->obj->getAllParams($record_id);
		return response()->json($dbresult);
	}


	public function getcopdAllParams ($record_id){
		//echo $record_id;exit();
		$dbresult = $this->obj->getcopdAllParamsmodel($record_id);
		return response()->json($dbresult);
	}

	public function getAllcopdParamsinfo($record_id){
		$dbresult = $this->obj->getcopdAllParams($record_id);
		return response()->json($dbresult);
	}
	
	public function getAllParamsOfResult($record_id){
		$dbresult = $this->obj->getAllParamsOfResult($record_id);
		return response()->json($dbresult);
	}

	public function changePatientStatus($id,$status){
        $userdata = array('screening_result' => $status);
        $data=DB::table('personal_info')->where("record_id",$id)->update($userdata);
         return array("status"=>true,"data"=> $data);
     }

}


