<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@login');
Route::get('/login', 'HomeController@login');
Route::get('/dashboard', 'HomeController@index');
Route::get('/index', 'HomeController@index');
Route::get('/add-result', 'HomeController@addResult');
Route::get('/leads', 'HomeController@leads');
Route::get('/copd-leads', 'HomeController@copdleads');
Route::get('/copd-data', 'HomeController@downloadDatacopd');
Route::post('/copd-filter', 'HomeController@copdfilter');
Route::get('/get-allcopd-params/{record_id}', 'HomeController@getAllcopdParamsinfo');
Route::get('/download-data-copd', 'HomeController@downloadcopdData');
Route::get('/download-media-copd', 'HomeController@downloadccopdMediacpd');



Route::get('/result', 'HomeController@result');
Route::get('/copd-result', 'HomeController@copdresult');
Route::get('/allusers', 'HomeController@user');
Route::get('/reports', 'HomeController@reporats');
Route::get('/copd-reports', 'HomeController@copdreporats');
Route::get('/copd-reports-mobile', 'HomeController@copdreporatsMobile');
Route::get('/download', 'HomeController@downloadLeads');
Route::get('/download-data', 'HomeController@downloadData');
Route::get('/download-media', 'HomeController@downloadMedia');

Route::get('/add-user', 'HomeController@addUser');

Route::post('/logincon', 'HomeController@logincon');
Route::get('/logout', 'HomeController@logout');


 

Route::get('/accupation','MasterdataController@Accupation');
Route::get('/supplyment','MasterdataController@Supplyment');
Route::get('/othercough','MasterdataController@othercough');

// Organation Routes
Route::get('/organation-list','MasterdataController@OrganationList');
Route::get('/organation','MasterdataController@Addorganation');
Route::post('/addOrganizationdata', 'MasterdataController@addOrganizationcontroll');
Route::get('/deleteOrganization/{id}', 'MasterdataController@deleteOrganization');



// Route::post('/deleteUser','MasterdataController@deleteUser');
Route::get('/deleteUser/{record_id}', 'HomeController@deleteUser');
Route::get('/deleteCopdLead/{record_id}', 'HomeController@deleteCopdLead');
Route::get('/deleteUserRoles/{record_id}', 'HomeController@deleteUserRoles');

Route::post('/filter', 'HomeController@filter');

// Route::post('/ajaxSend', ['as'=> 'ajaxSendmsg', 'uses'=>'HomeController@downloadData']);


//add user date_add
Route::post('/addUserdata', 'HomeController@addUserdata');

//resultfilter
Route::post('/resultfilter', 'HomeController@resultfilter');

Route::get('/getAllParams/{record_id}', 'HomeController@getAllParams');
Route::get('/copd-getAllParams/{record_id}', 'HomeController@getcopdAllParams');
Route::get('/getAllParamsOfResult/{record_id}', 'HomeController@getAllParamsOfResult');



Route::get('/getAllResults', 'HomeController@getAllResults');
Route::post('/checkLogin', 'HomeController@checkLogin');

Route::get('/insetResult/{record_id}', 'HomeController@insetResult');

Route::get('/api/change-patient-status/{id}/{status}', 'HomeController@changePatientStatus');