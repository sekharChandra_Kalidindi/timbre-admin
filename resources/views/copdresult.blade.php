<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Timbre admin</title>
    <link rel="icon" type="icon/css" href="{{ asset('images/appicon.png') }}">

    <!-- Bootstrap -->
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!--side menu plugin-->
    <link href="{{ asset('plugins/hoe-nav/hoe.css ') }}" rel="stylesheet">
    <!-- icons-->
    <link href="{{ asset('plugins/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet" />
    <link href="{{ asset('plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
    <!--template custom css file-->
    <link href="{{ asset('css/style.css')}}" rel="stylesheet">

    <script src="{{ asset('js/modernizr.js')}}"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
            <style>
                .input-group.margin-b-20 {
                    width: 100%;
                }
            </style>
</head>

<body hoe-navigation-type="vertical" hoe-nav-placement="left" theme-layout="wide-layout">

    <!--side navigation start-->
    <div id="hoeapp-wrapper" class="hoe-hide-lpanel" hoe-device-type="desktop">
        @include('includes.header')
        <div id="hoeapp-container" hoe-color-type="lpanel-bg7" hoe-lpanel-effect="shrink">

            @include('includes.sidemenu')

            <!--start main content-->
            <section id="main-content">
                <div class="space-30"></div>
                <!-- @yeld('content') -->
                <div class="container">
                    <div class="row">
                      
                    </div>

                    <div class="row">

                        <div class="col-md-12">
                            <div class="panel table-top">

                                <header class="panel-heading" style="display: table; width: 100%;">
                                    <h2 style="float: left;    line-height: 20px;" class="panel-title">All Result</h2>

                                </header>

                                <div style="padding-top:20px;">
                           <!-- <div class="col-md-8">
                              <div class="row">
                                 <div class="col-md-12">
                                    <form method="post" action="{{url('/resultfilter')}}">
                                       <input type="hidden" value="search" name="filter_type">
                                       <div class="row">
                                        
                                          <div class="col-md-3">
                                             <input id="searc_box"  value="{{$searchdata}}"  type="text"  name="src_string" placeholder="Search by patient name" class="form-control">
                                          </div>
                                          <div class="col-md-3">
                                             <select  class="form-control margin-b-20" name="org">
                                                <option value="">select organization</option>
                                                @foreach ($dbresult2 as $i=>$row)
                                                 @if($org==$row->organization)
                                                  <option selected>{{$row->organization}}</option>
                                                @else
                                                <option >{{$row->organization}}</option>
                                                @endif
                                                @endforeach
                                             </select>
                                          </div>
                                          <div class="col-md-5">
                                          <select  class="form-control margin-b-20" name="status">
                                                <option value="">select Status</option>
                                                @if($status=="1")
                                                <option value="1" selected>Yes(1)</option>
                                                @else
                                                <option value="1">Yes(1)</option>
                                                @endif
                                                @if($status=="0")
                                                <option value="0" selected>NO(0)</option>

                                                    @else
                                                    <option value="0">NO(0)</option>
                                                    @endif
                                                
                                             
                                             </select>
                                          </div>
                                          <div class="col-md-1">
                                             <button id="searc_Btn" type="submit" class="btn btn-primary">Search
                                             </button>
                                          </div>
                                       </div>
                                       
                                    </form>
                                 </div>
                             
                              </div>
                           </div>
                           <div class="col-md-4">
                              <div class="">
                                 <div class="col-md-12 ">
                                    <div class="form-group pull-right">
                                    <form method="post" action="{{url('/resultfilter')}}">
                                       <div class="input-group margin-b-20">
                                          <input value="{{$pid}}"  class="form-control" type="text" name="pid" placeholder="Patient Id"/>
                                          <span class="input-group-btn input-group-btn1">
                                          <button type="submit" id="download_data" class="btn btn-primary">Search
                                          </button>
                                        
                                          </span>
                                       </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </div> -->

                          
                        </div>

                                <div class="panel-body">
                                    <!-- <a class="pull-right don-btn" href="{{url('add-result(non-productive)')}}"> Add Result </a> -->
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Patient ID</th>
                                                <th>BMI</th>

                                                <th>Gender</th>
                                                <th>Age</th>
                                               
                                                <!-- <th>Organization</th> -->
                                                <th>Result from COPD</th>
                                                <!-- <th>Actions</th> -->
                                            </tr>
                                        </thead>
                                        <tbody id="tbodyd">
                                            @foreach ($dbresult as $i=>$row)
                                            <?php  $ss= $row->Target_Status == "0" ? "Dry cough" : "Wet cough"; ?>
                                                <tr>
                                                    <td scope="row">{{$i++}}</td>
                                                    <td>{{$row->patient_id}}</td>
                                                    <td>{{$row->BMI}}</td>
                                                    <td>{{$row->gender}}</td>
                                                    <td>{{$row->age}}</td>
                                                    
                                                    <!-- <td>{{$row->age}}</td>copd-reports?id={{$row->patient_id}} -->
                                                    <td>
                                                        <a class="asdf " href="javascript::void()"> {{$row->Target_Status}}  ({{$ss}})</a>
                                                    </td>
                                                    <!-- <td>
                                                        <div class="">
                                                        <a class="btn btn-xs btn-default viewDetails hide"  data-id="{{$row->patient_id}}"><i class="ion-eye"></i> View </a>
                                                        
                                                            <a class="btn btn-xs btn-default" href="copd-reports?id={{$row->patient_id}}"><i class="ion-eye"></i> click here See more details</a>
                                                        </div>
                                                    </td> -->

                                                </tr>
                                                @endforeach

                                        </tbody>
                                    </table>
                                </div>

                            </div>

                            <div class="col-md-12">

                                <div class="center">
                                @if($dbresult instanceof  \Illuminate\Pagination\LengthAwarePaginator)
                                 {{ $dbresult->links() }}
                                @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--col end-->
                </div>

        </div>
        <!--end container-->

        <!--footer start-->
        <div class="footer">
            <div class="row">
                <div class="col-sm-12">
                    <span>&copy; Copyright 2016. Timber</span>
                </div>
            </div>
        </div>
        <!--footer end-->
        </section>
        <!--end main content-->
    </div>
    </div>
    <!--end wrapper-->
    <div id="myModal" class="modal fade myModal" role="dialog">
              <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">More Details</h4>
                  </div>
                  <div class="modal-body" id="modalBody">
                
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>


    <!--Common plugins-->
    <script src="{{ asset('plugins/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('plugins/hoe-nav/hoe.js')}}"></script>
    <script src="{{ asset('plugins/pace/pace.min.js')}}"></script>
    <script src="{{ asset('plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('js/app1.js')}}"></script>
    <!--page scripts-->
    <!-- Flot chart js -->
    <!-- <script src="{{ asset('plugins/flot/jquery.flot.js')}}"></script>
    <script src="{{ asset('plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
    <script src="{{ asset('plugins/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{ asset('plugins/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{ asset('plugins/flot/jquery.flot.time.js')}}"></script> -->
    <!--vector map-->
    <!-- <script src="{{ asset('plugins/vectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
    <script src="{{ asset('plugins/vectormap/jquery-jvectormap-world-mill-en.js')}}"></script> -->
    <!-- ChartJS-->
    <!-- <script src="{{ asset('plugins/chartJs/Chart.min.js')}}"></script> -->
    <!--dashboard custom script-->
    <!-- <script src="{{ asset('js/dashboard.js')}}"></script> -->
</body>
<script>
  $( document ).ready(function() {
                    function modelListComponent(id,columns,data,numberOfColumns){
                        numberOfColumns=numberOfColumns||3;
                        let width='33.33%';
                        if(numberOfColumns===2){
                            width='50%';
                        }
                        $(`#${id}`).empty();
                        let lis=columns.map(c=>`<li style="width:${width}">
                        <strong>${c.title}</strong>
                        <p>${data[c.key] || "----"}</p>
                        </li>`).join('');
                        let htmlStr=`<ul class="modal-list-items">${lis}</ul>`;
                        $(`#${id}`).append(htmlStr)
                     }
                let leadsColumns=[{"title":"Patient_ID","key":"Patient_ID"},{"title":"Probability_score","key":"Probability_score"},{"title":"Predicted_Target_status","key":"Predicted_Target_status"},{"title":"Name","key":"Name"},{"title":"Gender","key":"Gender"},{"title":"Age","key":"Age"},{"title":"Height","key":"Height"},{"title":"Weight","key":"Weight"},{"title":"Occupation","key":"Occupation"},{"title":"BMI","key":"BMI"},{"title":"Smoking","key":"Smoking"},{"title":"Marital_Status","key":"Marital_Status"},{"title":"PreExisting_Conditions","key":"PreExisting_Conditions"},{"title":"Alcohol","key":"Alcohol"},{"title":"Current_Medications","key":"Current_Medications"},{"title":"Cough_Type","key":"Cough_Type"},{"title":"AppetitePattern","key":"AppetitePattern"},{"title":"x200Hz_LEFT_SUM","key":"x200Hz_LEFT_SUM"},{"title":"x200_500Hz_LEFT_SUM","key":"x200_500Hz_LEFT_SUM"},{"title":"x500_1000Hz_LEFT_SUM","key":"x500_1000Hz_LEFT_SUM"},{"title":"x1000_1500Hz_LEFT_SUM","key":"x1000_1500Hz_LEFT_SUM"},{"title":"x1500_2000Hz_LEFT_SUM","key":"x1500_2000Hz_LEFT_SUM"},{"title":"x2000_2500Hz_LEFT_SUM","key":"x2000_2500Hz_LEFT_SUM"},{"title":"x2500_3000Hz_LEFT_SUM","key":"x2500_3000Hz_LEFT_SUM"},{"title":"x3000_3500Hz_LEFT_SUM","key":"x3000_3500Hz_LEFT_SUM"},{"title":"x3500_4000Hz_LEFT_SUM","key":"x3500_4000Hz_LEFT_SUM"},{"title":"x4000_4500Hz_LEFT_SUM",
                "key":"x4000_4500Hz_LEFT_SUM"},{"title":"x4500_5000Hz_LEFT_SUM","key":"x4500_5000Hz_LEFT_SUM"},{"title":"x200Hz_LEFT_MEAN","key":"x200Hz_LEFT_MEAN"},{"title":"x200_500Hz_LEFT_MEAN","key":"x200_500Hz_LEFT_MEAN"},{"title":"x500_1000Hz_LEFT_MEAN","key":"x500_1000Hz_LEFT_MEAN"},{"title":"x1000_1500Hz_LEFT_MEAN","key":"x1000_1500Hz_LEFT_MEAN"},{"title":"x1500_2000Hz_LEFT_MEAN","key":"x1500_2000Hz_LEFT_MEAN"},{"title":"x2000_2500Hz_LEFT_MEAN","key":"x2000_2500Hz_LEFT_MEAN"},{"title":"x2500_3000Hz_LEFT_MEAN","key":"x2500_3000Hz_LEFT_MEAN"},{"title":"x3000_3500Hz_LEFT_MEAN","key":"x3000_3500Hz_LEFT_MEAN"},{"title":"x3500_4000Hz_LEFT_MEAN","key":"x3500_4000Hz_LEFT_MEAN"},{"title":"x4000_4500Hz_LEFT_MEAN","key":"x4000_4500Hz_LEFT_MEAN"},{"title":"x4500_5000Hz_LEFT_MEAN","key":"x4500_5000Hz_LEFT_MEAN"},{"title":"x200Hz_LEFT_SD","key":"x200Hz_LEFT_SD"},{"title":"x200_500Hz_LEFT_SD","key":"x200_500Hz_LEFT_SD"},{"title":"x500_1000Hz_LEFT_SD","key":"x500_1000Hz_LEFT_SD"},{"title":"x1000_1500Hz_LEFT_SD","key":"x1000_1500Hz_LEFT_SD"},{"title":"x1500_2000Hz_LEFT_SD","key":"x1500_2000Hz_LEFT_SD"},{"title":"x2000_2500Hz_LEFT_SD","key":"x2000_2500Hz_LEFT_SD"},{"title":"x2500_3000Hz_LEFT_SD","key":"x2500_3000Hz_LEFT_SD"},{"title":"x3000_3500Hz_LEFT_SD","key":"x3000_3500Hz_LEFT_SD"},{"title":"x3500_4000Hz_LEFT_SD","key":"x3500_4000Hz_LEFT_SD"},{"title":"x4000_4500Hz_LEFT_SD","key":"x4000_4500Hz_LEFT_SD"},{"title":"x4500_5000Hz_LEFT_SD","key":"x4500_5000Hz_LEFT_SD"},{"title":"x200Hz_LEFT_VARIANCE","key":"x200Hz_LEFT_VARIANCE"},{"title":"x200_500Hz_LEFT_VARIANCE","key":"x200_500Hz_LEFT_VARIANCE"},{"title":"x500_1000Hz_LEFT_VARIANCE","key":"x500_1000Hz_LEFT_VARIANCE"},{"title":"x1000_1500Hz_LEFT_VARIANCE","key":"x1000_1500Hz_LEFT_VARIANCE"},{"title":"x1500_2000Hz_LEFT_VARIANCE","key":"x1500_2000Hz_LEFT_VARIANCE"},{"title":"x2000_2500Hz_LEFT_VARIANCE","key":"x2000_2500Hz_LEFT_VARIANCE"},{"title":"x2500_3000Hz_LEFT_VARIANCE","key":"x2500_3000Hz_LEFT_VARIANCE"},{"title":"x3000_3500Hz_LEFT_VARIANCE","key":"x3000_3500Hz_LEFT_VARIANCE"},{"title":"x3500_4000Hz_LEFT_VARIANCE","key":"x3500_4000Hz_LEFT_VARIANCE"},{"title":"x4000_4500Hz_LEFT_VARIANCE","key":"x4000_4500Hz_LEFT_VARIANCE"},{"title":"x4500_5000Hz_LEFT_VARIANCE","key":"x4500_5000Hz_LEFT_VARIANCE"},{"title":"x200Hz_LEFT_COEFF_LEFT_VARIANCE","key":"x200Hz_LEFT_COEFF_LEFT_VARIANCE"},{"title":"x200_500Hz_LEFT_COEFF_LEFT_VARIANCE","key":"x200_500Hz_LEFT_COEFF_LEFT_VARIANCE"},{"title":"x500_1000Hz_LEFT_COEFF_LEFT_VARIANCE","key":"x500_1000Hz_LEFT_COEFF_LEFT_VARIANCE"},{"title":"x1000_1500Hz_LEFT_COEFF_LEFT_VARIANCE","key":"x1000_1500Hz_LEFT_COEFF_LEFT_VARIANCE"},{"title":"x1500_2000Hz_LEFT_COEFF_LEFT_VARIANCE","key":"x1500_2000Hz_LEFT_COEFF_LEFT_VARIANCE"},{"title":"x2000_2500Hz_LEFT_COEFF_LEFT_VARIANCE","key":"x2000_2500Hz_LEFT_COEFF_LEFT_VARIANCE"},{"title":"x2500_3000Hz_LEFT_COEFF_LEFT_VARIANCE","key":"x2500_3000Hz_LEFT_COEFF_LEFT_VARIANCE"},{"title":"x3000_3500Hz_LEFT_COEFF_LEFT_VARIANCE","key":"x3000_3500Hz_LEFT_COEFF_LEFT_VARIANCE"},{"title":"x3500_4000Hz_LEFT_COEFF_LEFT_VARIANCE","key":"x3500_4000Hz_LEFT_COEFF_LEFT_VARIANCE"},{"title":"x4000_4500Hz_LEFT_COEFF_LEFT_VARIANCE","key":"x4000_4500Hz_LEFT_COEFF_LEFT_VARIANCE"},{"title":"x4500_5000Hz_LEFT_COEFF_LEFT_VARIANCE","key":"x4500_5000Hz_LEFT_COEFF_LEFT_VARIANCE"},{"title":"x200Hz_LEFT_TOP10_LEFT_AMPLITUDE","key":"x200Hz_LEFT_TOP10_LEFT_AMPLITUDE"},{"title":"x200_500Hz_LEFT_TOP10_LEFT_AMPLITUDE","key":"x200_500Hz_LEFT_TOP10_LEFT_AMPLITUDE"},{"title":"x500_1000Hz_LEFT_TOP10_LEFT_AMPLITUDE","key":"x500_1000Hz_LEFT_TOP10_LEFT_AMPLITUDE"},{"title":"x1000_1500Hz_LEFT_TOP10_LEFT_AMPLITUDE","key":"x1000_1500Hz_LEFT_TOP10_LEFT_AMPLITUDE"},{"title":"x1500_2000Hz_LEFT_TOP10_LEFT_AMPLITUDE","key":"x1500_2000Hz_LEFT_TOP10_LEFT_AMPLITUDE"},{"title":"x2000_2500Hz_LEFT_TOP10_LEFT_AMPLITUDE","key":"x2000_2500Hz_LEFT_TOP10_LEFT_AMPLITUDE"},{"title":"x2500_3000Hz_LEFT_TOP10_LEFT_AMPLITUDE","key":"x2500_3000Hz_LEFT_TOP10_LEFT_AMPLITUDE"},{"title":"x3000_3500Hz_LEFT_TOP10_LEFT_AMPLITUDE","key":"x3000_3500Hz_LEFT_TOP10_LEFT_AMPLITUDE"},{"title":"x3500_4000Hz_LEFT_TOP10_LEFT_AMPLITUDE","key":"x3500_4000Hz_LEFT_TOP10_LEFT_AMPLITUDE"},{"title":"x4000_4500Hz_LEFT_TOP10_LEFT_AMPLITUDE","key":"x4000_4500Hz_LEFT_TOP10_LEFT_AMPLITUDE"},{"title":"x4500_5000Hz_LEFT_TOP10_LEFT_AMPLITUDE","key":"x4500_5000Hz_LEFT_TOP10_LEFT_AMPLITUDE"},{"title":"x200Hz_LEFT_SPECTRAL_LEFT_CENTROID","key":"x200Hz_LEFT_SPECTRAL_LEFT_CENTROID"},{"title":"x200_500Hz_LEFT_SPECTRAL_LEFT_CENTROID","key":"x200_500Hz_LEFT_SPECTRAL_LEFT_CENTROID"},{"title":"x500_1000Hz_LEFT_SPECTRAL_LEFT_CENTROID","key":"x500_1000Hz_LEFT_SPECTRAL_LEFT_CENTROID"},{"title":"x1000_1500Hz_LEFT_SPECTRAL_LEFT_CENTROID","key":"x1000_1500Hz_LEFT_SPECTRAL_LEFT_CENTROID"},{"title":"x1500_2000Hz_LEFT_SPECTRAL_LEFT_CENTROID","key":"x1500_2000Hz_LEFT_SPECTRAL_LEFT_CENTROID"},{"title":"x2000_2500Hz_LEFT_SPECTRAL_LEFT_CENTROID","key":"x2000_2500Hz_LEFT_SPECTRAL_LEFT_CENTROID"},{"title":"x2500_3000Hz_LEFT_SPECTRAL_LEFT_CENTROID","key":"x2500_3000Hz_LEFT_SPECTRAL_LEFT_CENTROID"},{"title":"x3000_3500Hz_LEFT_SPECTRAL_LEFT_CENTROID","key":"x3000_3500Hz_LEFT_SPECTRAL_LEFT_CENTROID"},{"title":"x3500_4000Hz_LEFT_SPECTRAL_LEFT_CENTROID","key":"x3500_4000Hz_LEFT_SPECTRAL_LEFT_CENTROID"},{"title":"x4000_4500Hz_LEFT_SPECTRAL_LEFT_CENTROID","key":"x4000_4500Hz_LEFT_SPECTRAL_LEFT_CENTROID"},{"title":"x4500_5000Hz_LEFT_SPECTRAL_LEFT_CENTROID","key":"x4500_5000Hz_LEFT_SPECTRAL_LEFT_CENTROID"},{"title":"x200Hz_LEFT_SPECTRAL_LEFT_FLATNESS","key":"x200Hz_LEFT_SPECTRAL_LEFT_FLATNESS"},{"title":"x200_500Hz_LEFT_SPECTRAL_LEFT_FLATNESS","key":"x200_500Hz_LEFT_SPECTRAL_LEFT_FLATNESS"},{"title":"x500_1000Hz_LEFT_SPECTRAL_LEFT_FLATNESS","key":"x500_1000Hz_LEFT_SPECTRAL_LEFT_FLATNESS"},{"title":"x1000_1500Hz_LEFT_SPECTRAL_LEFT_FLATNESS","key":"x1000_1500Hz_LEFT_SPECTRAL_LEFT_FLATNESS"},{"title":"x1500_2000Hz_LEFT_SPECTRAL_LEFT_FLATNESS","key":"x1500_2000Hz_LEFT_SPECTRAL_LEFT_FLATNESS"},{"title":"x2000_2500Hz_LEFT_SPECTRAL_LEFT_FLATNESS","key":"x2000_2500Hz_LEFT_SPECTRAL_LEFT_FLATNESS"},{"title":"x2500_3000Hz_LEFT_SPECTRAL_LEFT_FLATNESS","key":"x2500_3000Hz_LEFT_SPECTRAL_LEFT_FLATNESS"},{"title":"x3000_3500Hz_LEFT_SPECTRAL_LEFT_FLATNESS","key":"x3000_3500Hz_LEFT_SPECTRAL_LEFT_FLATNESS"},{"title":"x3500_4000Hz_LEFT_SPECTRAL_LEFT_FLATNESS","key":"x3500_4000Hz_LEFT_SPECTRAL_LEFT_FLATNESS"},{"title":"x4000_4500Hz_LEFT_SPECTRAL_LEFT_FLATNESS","key":"x4000_4500Hz_LEFT_SPECTRAL_LEFT_FLATNESS"},{"title":"x4500_5000Hz_LEFT_SPECTRAL_LEFT_FLATNESS","key":"x4500_5000Hz_LEFT_SPECTRAL_LEFT_FLATNESS"},{"title":"x200Hz_LEFT_SPECTRAL_LEFT_SKEWNESS","key":"x200Hz_LEFT_SPECTRAL_LEFT_SKEWNESS"},{"title":"x200_500Hz_LEFT_SPECTRAL_LEFT_SKEWNESS","key":"x200_500Hz_LEFT_SPECTRAL_LEFT_SKEWNESS"},{"title":"x500_1000Hz_LEFT_SPECTRAL_LEFT_SKEWNESS","key":"x500_1000Hz_LEFT_SPECTRAL_LEFT_SKEWNESS"},{"title":"x1000_1500Hz_LEFT_SPECTRAL_LEFT_SKEWNESS","key":"x1000_1500Hz_LEFT_SPECTRAL_LEFT_SKEWNESS"},{"title":"x1500_2000Hz_LEFT_SPECTRAL_LEFT_SKEWNESS","key":"x1500_2000Hz_LEFT_SPECTRAL_LEFT_SKEWNESS"},{"title":"x2000_2500Hz_LEFT_SPECTRAL_LEFT_SKEWNESS","key":"x2000_2500Hz_LEFT_SPECTRAL_LEFT_SKEWNESS"},{"title":"x2500_3000Hz_LEFT_SPECTRAL_LEFT_SKEWNESS","key":"x2500_3000Hz_LEFT_SPECTRAL_LEFT_SKEWNESS"},{"title":"x3000_3500Hz_LEFT_SPECTRAL_LEFT_SKEWNESS","key":"x3000_3500Hz_LEFT_SPECTRAL_LEFT_SKEWNESS"},{"title":"x3500_4000Hz_LEFT_SPECTRAL_LEFT_SKEWNESS","key":"x3500_4000Hz_LEFT_SPECTRAL_LEFT_SKEWNESS"},{"title":"x4000_4500Hz_LEFT_SPECTRAL_LEFT_SKEWNESS","key":"x4000_4500Hz_LEFT_SPECTRAL_LEFT_SKEWNESS"},{"title":"x4500_5000Hz_LEFT_SPECTRAL_LEFT_SKEWNESS","key":"x4500_5000Hz_LEFT_SPECTRAL_LEFT_SKEWNESS"},{"title":"x200Hz_LEFT_KURTOSIS","key":"x200Hz_LEFT_KURTOSIS"},{"title":"x200_500Hz_LEFT_KURTOSIS","key":"x200_500Hz_LEFT_KURTOSIS"},{"title":"x500_1000Hz_LEFT_KURTOSIS","key":"x500_1000Hz_LEFT_KURTOSIS"},{"title":"x1000_1500Hz_LEFT_KURTOSIS","key":"x1000_1500Hz_LEFT_KURTOSIS"},{"title":"x1500_2000Hz_LEFT_KURTOSIS","key":"x1500_2000Hz_LEFT_KURTOSIS"},{"title":"x2000_2500Hz_LEFT_KURTOSIS","key":"x2000_2500Hz_LEFT_KURTOSIS"},{"title":"x2500_3000Hz_LEFT_KURTOSIS","key":"x2500_3000Hz_LEFT_KURTOSIS"},{"title":"x3000_3500Hz_LEFT_KURTOSIS","key":"x3000_3500Hz_LEFT_KURTOSIS"},{"title":"x3500_4000Hz_LEFT_KURTOSIS","key":"x3500_4000Hz_LEFT_KURTOSIS"},{"title":"x4000_4500Hz_LEFT_KURTOSIS","key":"x4000_4500Hz_LEFT_KURTOSIS"},{"title":"x4500_5000Hz_LEFT_KURTOSIS","key":"x4500_5000Hz_LEFT_KURTOSIS"},{"title":"x200Hz_LEFT_MFCC","key":"x200Hz_LEFT_MFCC"},{"title":"x200_500Hz_LEFT_MFCC","key":"x200_500Hz_LEFT_MFCC"},{"title":"x500_1000Hz_LEFT_MFCC",
                "key":"x500_1000Hz_LEFT_MFCC"},{"title":"x1000_1500Hz_LEFT_MFCC","key":"x1000_1500Hz_LEFT_MFCC"},{"title":"x1500_2000Hz_LEFT_MFCC","key":"x1500_2000Hz_LEFT_MFCC"},{"title":"x2000_2500Hz_LEFT_MFCC","key":"x2000_2500Hz_LEFT_MFCC"},{"title":"x2500_3000Hz_LEFT_MFCC","key":"x2500_3000Hz_LEFT_MFCC"},{"title":"x3000_3500Hz_LEFT_MFCC","key":"x3000_3500Hz_LEFT_MFCC"},{"title":"x3500_4000Hz_LEFT_MFCC","key":"x3500_4000Hz_LEFT_MFCC"},{"title":"x4000_4500Hz_LEFT_MFCC","key":"x4000_4500Hz_LEFT_MFCC"},{"title":"x4500_5000Hz_LEFT_MFCC","key":"x4500_5000Hz_LEFT_MFCC"},{"title":"x200Hz_LEFT_Energy","key":"x200Hz_LEFT_Energy"},{"title":"x200_500Hz_LEFT_Energy","key":"x200_500Hz_LEFT_Energy"},{"title":"x500_1000Hz_LEFT_Energy","key":"x500_1000Hz_LEFT_Energy"},{"title":"x1000_1500Hz_LEFT_Energy","key":"x1000_1500Hz_LEFT_Energy"},{"title":"x1500_2000Hz_LEFT_Energy","key":"x1500_2000Hz_LEFT_Energy"},{"title":"x2000_2500Hz_LEFT_Energy","key":"x2000_2500Hz_LEFT_Energy"},{"title":"x2500_3000Hz_LEFT_Energy","key":"x2500_3000Hz_LEFT_Energy"},{"title":"x3000_3500Hz_LEFT_Energy","key":"x3000_3500Hz_LEFT_Energy"},{"title":"x3500_4000Hz_LEFT_Energy","key":"x3500_4000Hz_LEFT_Energy"},{"title":"x4000_4500Hz_LEFT_Energy","key":"x4000_4500Hz_LEFT_Energy"},{"title":"x4500_5000Hz_LEFT_Energy","key":"x4500_5000Hz_LEFT_Energy"}];
                $(".viewDetails").click(function(){
                    $.get(`getAllParamsOfResult/${$(this).attr('data-id')}`,function(res){
                        modelListComponent('modalBody',leadsColumns,(res[0] || {}),2);
                        $("#myModal").modal('show');
                    })
                })

                })
    //   $.ajax({
    //        url:"http://35.244.60.138/timber/api/index.php",
    //        method:"POST",
    //       data: JSON.stringify({"action":"GETD"}),
    //     //  "content-type": "application/json; charset=utf-8",
    //       "cache-control" : "no-cache",
    //      "processData": false,
    //       success:function (data){
    //            data=data.reverse();
    //          data.forEach(function(item,i){
    //               let ss= item['Predicted_Target_status'] === "0" ? "Dry cough(non-productive)(0)" : "Wet cough(productive)(1)"
    //              let result = `  <tr>
    //                                     <td scope="row">${i++}</td>
    //                                     <td>${item.Patient_ID}</td>
    //                                     <td>${item.Name}</td>
    //                                     <td>${item.Gender}</td>
    //                                     <td>${item.Age}</td>
    //                                     <td>${item.BMI}</td>
    //                                     <td>
    //                                      <a class="asdf" href="reports?id=${item.Patient_ID}"> ${ss}</a>
    //                                     </td>

    //                                 </tr>`;
    //                                 $("#tbodyd").append($(result));
    //          })
    //       }
    //   })
</script>

</html>