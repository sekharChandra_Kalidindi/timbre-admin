    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
            <title>Timbre admin</title>
            <link rel="icon"  type="icon/css" href="{{ asset('images/appicon.png') }}">

            <!-- Bootstrap -->
            <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
            <!--side menu plugin-->
            <link href="{{ asset('plugins/hoe-nav/hoe.css ') }}" rel="stylesheet">
            <!-- icons-->
            <link href="{{ asset('plugins/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
            <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
            <link href="{{ asset('plugins/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet" />
            <link href="{{ asset('plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
            <!--template custom css file-->
            <link href="{{ asset('css/style.css')}}" rel="stylesheet">

            <script src="{{ asset('js/modernizr.js')}}"></script>
            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
            <style>
                .logo-cont{
                    weight:100%;
                    margin-bottom:30px;
                }
                .logo-cont img{
                    margin:0px auto;
                    width: 230px;
                    display:block;
                }
            </style>
        </head>
        <body class="bg" hoe-navigation-type="vertical" hoe-nav-placement="left" theme-layout="wide-layout">

            <div class="page-center">
            <div class="page-center-in ">
                <form class="sign-box " action="{{url('logincon')}}" method="post">
                @if(session()->has('message'))              
                <div class="alert alert-success">{{session()->get('message')}}</div>
              @endif
                    <div class="sign-avatar">
                        <img src="assets/images/logo.png" alt="" >
                    </div>

                    <div class="logo-cont">
                        <img src="{{ asset('images/logo.png')}}"  alt="" class="img-responsive">
                    </div>

                    <header class="sign-title">Sign In</header>
                    <div class="form-group">
                        <input type="text" class="form-control" name="username" placeholder="E-Mail or Phone">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                    </div>
                    <div class="form-group clearfix">
                       <!--  <div class="checkbox pull-left">
                            <input type="checkbox" id="myCheckbox" name="myCheckbox" class="i-checks">
                            <label for="myCheckbox">Remember Me </label>
                        </div> -->
                        
                    </div>
                    <button class="btn btn-primary btnsuc cust-pad rounded btn-lg" type="submit">Login</button>
                    
                    <!-- <a href="{{url('dashboard')}}" type="submit"  class="btn btn-success cust-pad rounded btn-lg">Sign in</a> -->
                    
                </form>
            </div><!--page center in-->
        </div><!--page center-->

            <!--Common plugins-->
            <script src="{{ asset('plugins/jquery/dist/jquery.min.js')}}"></script>
            <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
            <script src="{{ asset('plugins/hoe-nav/hoe.js')}}"></script>
            <script src="{{ asset('plugins/pace/pace.min.js')}}"></script>
            <script src="{{ asset('plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
            <script src="{{ asset('js/app1.js')}}"></script>
            <!--page scripts-->
            <!-- Flot chart js -->
            <script src="{{ asset('plugins/flot/jquery.flot.js')}}"></script>
            <script src="{{ asset('plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
            <script src="{{ asset('plugins/flot/jquery.flot.resize.js')}}"></script>
            <script src="{{ asset('plugins/flot/jquery.flot.pie.js')}}"></script>
            <script src="{{ asset('plugins/flot/jquery.flot.time.js')}}"></script>
            <!--vector map-->
            <script src="{{ asset('plugins/vectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
            <script src="{{ asset('plugins/vectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
            <!-- ChartJS-->
            <script src="{{ asset('plugins/chartJs/Chart.min.js')}}"></script>
            <!--dashboard custom script-->
            <script src="{{ asset('js/dashboard.js')}}"></script>
        </body>
    </html>