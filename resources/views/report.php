    <!DOCTYPE html>
    <html lang="en">
        <head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
            
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
            <title>Timbre admin</title>
            <link rel="icon"  type="icon/css" href="{{ asset('images/appicon.png') }}">

            <!-- Bootstrap -->
            <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
            <!--side menu plugin-->
            <link href="{{ asset('plugins/hoe-nav/hoe.css ') }}" rel="stylesheet">
            <!-- icons-->
            <link href="{{ asset('plugins/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
            <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
            <link href="{{ asset('plugins/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet" />
            <link href="{{ asset('plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
            <!--template custom css file-->
            <link href="{{ asset('css/style.css')}}" rel="stylesheet">
            <link href="{{ asset('css/w3.css')}}" rel="stylesheet">

            <script src="{{ asset('js/modernizr.js')}}"></script>
            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
            <style>
            .loader-container{
                position: absolute;
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.12156862745098039);
    z-index: 99999999;
    margin-top: 50px;
            }
            .lds-hourglass {
        display: inline-block;
        position: relative;
        width: 64px;
        left:50%;
        top:30%;
        height: 64px;
        }
        .lds-hourglass:after {
        content: " ";
        display: block;
        border-radius: 50%;
        width: 0;
        height: 0;
        margin: 6px;
        box-sizing: border-box;
        border: 26px solid #fff;
        border-color: #fff transparent #fff transparent;
        animation: lds-hourglass 1.2s infinite;
        }
        @keyframes lds-hourglass {
        0% {
            transform: rotate(0);
            animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
        }
        50% {
            transform: rotate(900deg);
            animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
        }
        100% {
            transform: rotate(1800deg);
        }
        }
        .cc{
            font-size:18px;
            text-transform: capitalize;
        }

            </style>
        </head>
        <body hoe-navigation-type="vertical" hoe-nav-placement="left" theme-layout="wide-layout">

        <div class="loader-container">
        <div class="lds-hourglass"></div>
        </div>

            <!--side navigation start-->
            <div id="hoeapp-wrapper" class="hoe-hide-lpanel" hoe-device-type="desktop">
                @include('includes.header')
                <div id="hoeapp-container" hoe-color-type="lpanel-bg7" hoe-lpanel-effect="shrink">
                    
                @include('includes.sidemenu')

                    <!--start main content-->
                    <section id="main-content">
                        <div class="space-30"></div>
                        <!-- @yeld('content') -->
                        <div class="container">
                            
                            <div class="row">
                               
                                <div class="col-md-12">
                                    <div class="panel table-top">
                                        <header class="panel-heading">
                                            <h2 class="panel-title" style="color:#fff">Report Of Patent ID : <strong class="pid"></strong> </h2>
                                        </header>
                                        <div class="panel-body">
                                            <div class="w3-row">

            <!-- Blog entries -->
            <div class="w3-col l8 s12">
                <!-- Blog entry -->
                <div class="w3-card-4 w3-margin w3-white" style="min-height:400px; position:relative; ">

                    <div class="w3-container">
                        <div class="loader"></div>
                        <!-- <h3><b>Patient <span class="pid w3-opacity"></span> Report</b></h3> -->
                        <h5>Status: <span class="w3-opacity pstatus"></span></h5>
                        <div class="w3-col l6">
                          <strong>Params</strong>
                        <select class="w3-select w3-border new-select">
                        <option>select</option>
                   

                    </select>
                    </div>
                    <div class="w3-col l6">
                        <strong>Vizualization Type</strong>
                          <select class="w3-select w3-border viz-select">
                        <option>select</option>
                        <option value="area">area</option>
                        <option value="spline">line</option>
                   

                    </select>
                        </div>
                    </div>

                    <div id="container">

                    </div>
                </div>
                <hr>



            </div>
            <div class="w3-col l4">


                <!-- Posts -->
                <div class="w3-card w3-margin">
                    <div class="w3-container w3-padding" style="background:#f1f1f1">
                        <h4>Patient Details</h4>
                    </div>
                    <ul class="w3-ul w3-hoverable w3-white">
                        <li class="w3-padding-16">

                            <span class="w3-large w3-label">Gender  </span> 
                             :   <span class="gender cc "></span>
                        </li>
                        <li class="w3-padding-16">

                            <span class="w3-large w3-label">Age </span>
                             :   <span class="age cc"></span>
                        </li>
                        <li class="w3-padding-16">

                            <span class="w3-large w3-label">BMI  </span>
                             :  <span class="bmi cc "></span>
                        </li>
                        <li class="w3-padding-16 w3-hide-medium w3-hide-small">
                            <span class="w3-large w3-label">AppetitePattern</span>
                             :  <span class="AppetitePattern cc"></span>
                        </li>
                           <li class="w3-padding-16 w3-hide-medium w3-hide-small">
                            <span class="w3-large w3-label">Cough Type</span>
                             :  <span class="Cough_Type cc "></span>
                        </li>
                           <li class="w3-padding-16 w3-hide-medium w3-hide-small">
                            <span class="w3-large w3-label">Weight x Height  </span>
                             :   <span class="wh cc"></span>
                        </li>

                           <li class="w3-padding-16 w3-hide-medium w3-hide-small">
                            <span class="w3-large w3-label">Current Medications</span>
                             :  <span class="Current_Medications cc"></span>
                            </li>
                           <!-- <li class="w3-padding-16 w3-hide-medium w3-hide-small">
                            <span class="w3-large w3-label">Occupation</span>
                             :  <span class="Occupation cc"></span>
                        </li> -->
                        <!-- <li class="w3-padding-16 w3-hide-medium w3-hide-small">
                            <span class="w3-large w3-label">Appetite Pattern</span>
                             :  <span class="AppetitePattern cc"></span>
                        </li> -->
                          

                    </ul>
                </div>
                <hr>
                <!-- About Card -->
                <div class="w3-card w3-margin w3-margin-top">

                  
                </div>
                <hr>

                <!-- END Introduction Menu -->
            </div>


            <!-- END GRID -->
        </div><br>

                                           </div>

                                        <div class="col-md-12">
                                            
                                            <div class="center">
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div><!--col end-->
                            </div>


                        </div><!--end container-->

                        <!--footer start-->
                        <div class="footer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <span>&copy; Copyright 2016. Timber</span>
                                </div>
                            </div>
                        </div>
                        <!--footer end-->
                    </section><!--end main content-->
                </div>
            </div><!--end wrapper-->

            <!--Common plugins-->
            <script src="{{ asset('plugins/jquery/dist/jquery.min.js')}}"></script>
            <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
            <script src="{{ asset('plugins/hoe-nav/hoe.js')}}"></script>
            <script src="{{ asset('plugins/pace/pace.min.js')}}"></script>
            <script src="{{ asset('plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
            <script src="{{ asset('js/app1.js')}}"></script>
            <!--page scripts-->
            <!-- Flot chart js -->
            <script src="{{ asset('plugins/flot/jquery.flot.js')}}"></script>
            <script src="{{ asset('plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
            <script src="{{ asset('plugins/flot/jquery.flot.resize.js')}}"></script>
            <script src="{{ asset('plugins/flot/jquery.flot.pie.js')}}"></script>
            <script src="{{ asset('plugins/flot/jquery.flot.time.js')}}"></script>
            <!--vector map-->
            <script src="{{ asset('plugins/vectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
            <script src="{{ asset('plugins/vectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
            <!-- ChartJS-->
            <script src="{{ asset('plugins/chartJs/Chart.min.js')}}"></script>
            <!--dashboard custom script-->
            <script src="{{ asset('js/dashboard.js')}}"></script>
            <!-- <script src="{{ asset('js/highcharts.js')}}"></script>
            <script src="{{ asset('js/exporting.js')}}"></script>
            <script src="{{ asset('js/export-data.js')}}"></script> -->
            <script src="https://code.highcharts.com/highcharts.js"></script>
            <script src="https://code.highcharts.com/modules/parallel-coordinates.js"></script>

            <script>
let keysss=[{
    "key": "LEFT_SUM",
    "x": [
        // "x200Hz_LEFT_SUM",
        // "x200_500Hz_LEFT_SUM",
        // "x500_1000Hz_LEFT_SUM",
        // "x1000_1500Hz_LEFT_SUM",
        // "x1500_2000Hz_LEFT_SUM",
        // "x2000_2500Hz_LEFT_SUM",
        // "x2500_3000Hz_LEFT_SUM",
        "x3000_3500Hz_LEFT_SUM",
        "x3500_4000Hz_LEFT_SUM",
        "x4000_4500Hz_LEFT_SUM",
        "x4500_5000Hz_LEFT_SUM"
    ]
}
, {
    "key": "LEFT_MEAN",
    "x": [
        //"x200Hz_LEFT_MEANLEFT_MEAN",
        // "x200_500Hz_LEFT_MEAN",
        // "x500_1000Hz_LEFT_MEAN",
        // "x1000_1500Hz_LEFT_MEAN",
        // "x1500_2000Hz_LEFT_MEAN",
        // "x2000_2500Hz_LEFT_MEAN",
        // "x2500_3000Hz_LEFT_MEAN",
        "x3000_3500Hz_LEFT_MEAN",
        "x3500_4000Hz_LEFT_MEAN",
        "x4000_4500Hz_LEFT_MEAN",
        "x4500_5000Hz_LEFT_MEAN"
    ]

},
{
    "key": "LEFT_SD",
    "x": [
        // "x200Hz_LEFT_SD",
        // "x200_500Hz_LEFT_SD",
        // "x500_1000Hz_LEFT_SD",
        // "x1000_1500Hz_LEFT_SD",
        // "x1500_2000Hz_LEFT_SD",
        // "x2000_2500Hz_LEFT_SD",
        // "x2500_3000Hz_LEFT_SD",
        "x3000_3500Hz_LEFT_SD",
        "x3500_4000Hz_LEFT_SD",
        "x4000_4500Hz_LEFT_SD",
        "x4500_5000Hz_LEFT_SD",
    ]
}
, {
    "key": "LEFT_VARIANCE",
    "x": [
        // "x200Hz_LEFT_VARIANCE",
        // "x200_500Hz_LEFT_VARIANCE",
        // "x500_1000Hz_LEFT_VARIANCE",
        // "x1000_1500Hz_LEFT_VARIANCE",
        // "x1500_2000Hz_LEFT_VARIANCE",
        // "x2000_2500Hz_LEFT_VARIANCE",
        // "x2500_3000Hz_LEFT_VARIANCE",
        "x3000_3500Hz_LEFT_VARIANCE",
        "x3500_4000Hz_LEFT_VARIANCE",
        "x4000_4500Hz_LEFT_VARIANCE",
        "x4500_5000Hz_LEFT_VARIANCE"
    ]
}, {
    "key": "LEFT_COEFF_LEFT_VARIANCE",
    "x": [
        // "x200Hz_LEFT_COEFF_LEFT_VARIANCE",
        // "x200_500Hz_LEFT_COEFF_LEFT_VARIANCE",
        // "x500_1000Hz_LEFT_COEFF_LEFT_VARIANCE",
        // "x1000_1500Hz_LEFT_COEFF_LEFT_VARIANCE",
        // "x1500_2000Hz_LEFT_COEFF_LEFT_VARIANCE",
        // "x2000_2500Hz_LEFT_COEFF_LEFT_VARIANCE",
        // "x2500_3000Hz_LEFT_COEFF_LEFT_VARIANCE",
        "x3000_3500Hz_LEFT_COEFF_LEFT_VARIANCE",
        "x3500_4000Hz_LEFT_COEFF_LEFT_VARIANCE",
        "x4000_4500Hz_LEFT_COEFF_LEFT_VARIANCE",
        "x4500_5000Hz_LEFT_COEFF_LEFT_VARIANCE"
    ]

}, {
    "key": "LEFT_TOP10_LEFT_AMPLITUDE",
    "x": [
        // "x200Hz_LEFT_TOP10_LEFT_AMPLITUDE",
        // "x200_500Hz_LEFT_TOP10_LEFT_AMPLITUDE",
        // "x500_1000Hz_LEFT_TOP10_LEFT_AMPLITUDE",
        // "x1000_1500Hz_LEFT_TOP10_LEFT_AMPLITUDE",
        // "x1500_2000Hz_LEFT_TOP10_LEFT_AMPLITUDE",
        // "x2000_2500Hz_LEFT_TOP10_LEFT_AMPLITUDE",
        // "x2500_3000Hz_LEFT_TOP10_LEFT_AMPLITUDE",
        "x3000_3500Hz_LEFT_TOP10_LEFT_AMPLITUDE",
        "x3500_4000Hz_LEFT_TOP10_LEFT_AMPLITUDE",
        "x4000_4500Hz_LEFT_TOP10_LEFT_AMPLITUDE",
        "x4500_5000Hz_LEFT_TOP10_LEFT_AMPLITUDE"
    ]

}, {
    "key": "LEFT_SPECTRAL_LEFT_CENTROID",
    "x": [
    //    "x200Hz_LEFT_SPECTRAL_LEFT_CENTROID",
    //     "x200_500Hz_LEFT_SPECTRAL_LEFT_CENTROID",
    //     "x500_1000Hz_LEFT_SPECTRAL_LEFT_CENTROID",
    //     "x1000_1500Hz_LEFT_SPECTRAL_LEFT_CENTROID",
    //     "x1500_2000Hz_LEFT_SPECTRAL_LEFT_CENTROID",
    //     "x2000_2500Hz_LEFT_SPECTRAL_LEFT_CENTROID",
    //     "x2500_3000Hz_LEFT_SPECTRAL_LEFT_CENTROID",
        "x3000_3500Hz_LEFT_SPECTRAL_LEFT_CENTROID",
        "x3500_4000Hz_LEFT_SPECTRAL_LEFT_CENTROID",
        "x4000_4500Hz_LEFT_SPECTRAL_LEFT_CENTROID",
        "x4500_5000Hz_LEFT_SPECTRAL_LEFT_CENTROID",
        // "x200Hz_LEFT_SPECTRAL_LEFT_FLATNESS"
    ]
}, {
    "key": "LEFT_SPECTRAL_LEFT_FLATNESS",
    "x": [
        // "x200Hz_LEFT_SPECTRAL_LEFT_FLATNESS",
        // "x200_500Hz_LEFT_SPECTRAL_LEFT_FLATNESS",
        // "x500_1000Hz_LEFT_SPECTRAL_LEFT_FLATNESS",
        // "x1000_1500Hz_LEFT_SPECTRAL_LEFT_FLATNESS",
        // "x1500_2000Hz_LEFT_SPECTRAL_LEFT_FLATNESS",
        // "x2000_2500Hz_LEFT_SPECTRAL_LEFT_FLATNESS",
        // "x2500_3000Hz_LEFT_SPECTRAL_LEFT_FLATNESS",
        "x3000_3500Hz_LEFT_SPECTRAL_LEFT_FLATNESS",
        "x3500_4000Hz_LEFT_SPECTRAL_LEFT_FLATNESS",
        "x4000_4500Hz_LEFT_SPECTRAL_LEFT_FLATNESS",
        "x4500_5000Hz_LEFT_SPECTRAL_LEFT_FLATNESS"
    ]
}, {
    "key": "LEFT_SPECTRAL_LEFT_SKEWNESS",
    "x": [
        // "x200Hz_LEFT_SPECTRAL_LEFT_SKEWNESS",
        // "x200_500Hz_LEFT_SPECTRAL_LEFT_SKEWNESS",
        // "x500_1000Hz_LEFT_SPECTRAL_LEFT_SKEWNESS",
        // "x1000_1500Hz_LEFT_SPECTRAL_LEFT_SKEWNESS",
        // "x1500_2000Hz_LEFT_SPECTRAL_LEFT_SKEWNESS",
        // "x2000_2500Hz_LEFT_SPECTRAL_LEFT_SKEWNESS",
        // "x2500_3000Hz_LEFT_SPECTRAL_LEFT_SKEWNESS",
        "x3000_3500Hz_LEFT_SPECTRAL_LEFT_SKEWNESS",
        "x3500_4000Hz_LEFT_SPECTRAL_LEFT_SKEWNESS",
        "x4000_4500Hz_LEFT_SPECTRAL_LEFT_SKEWNESS",
        "x4500_5000Hz_LEFT_SPECTRAL_LEFT_SKEWNESS"
    ]
}, {
    "key": "LEFT_KURTOSIS",
    "x": [
        // "x200Hz_LEFT_KURTOSIS" ,
        // "x200_500Hz_LEFT_KURTOSIS",
        // "x500_1000Hz_LEFT_KURTOSIS",
        // "x1000_1500Hz_LEFT_KURTOSIS",
        // "x1500_2000Hz_LEFT_KURTOSIS",
        // "x2000_2500Hz_LEFT_KURTOSIS",
        // "x2500_3000Hz_LEFT_KURTOSIS",
        "x3000_3500Hz_LEFT_KURTOSIS",
        "x3500_4000Hz_LEFT_KURTOSIS",
        "x4000_4500Hz_LEFT_KURTOSIS",
        "x4500_5000Hz_LEFT_KURTOSIS"
    ]
}, {
    "key": "LEFT_MFCC",
    "x": [
        // "x200Hz_LEFT_MFCC",
        // "x200_500Hz_LEFT_MFCC",
        // "x500_1000Hz_LEFT_MFCC",
        // "x1000_1500Hz_LEFT_MFCC",
        // "x1500_2000Hz_LEFT_MFCC",
        // "x2000_2500Hz_LEFT_MFCC",
        // "x2500_3000Hz_LEFT_MFCC",
        "x3000_3500Hz_LEFT_MFCC",
        "x3500_4000Hz_LEFT_MFCC",
        "x4000_4500Hz_LEFT_MFCC",
        "x4500_5000Hz_LEFT_MFCC"
    ]
}, {
    "key": "LEFT_Energy",
    "x": [
        // "x200Hz_LEFT_Energy",
        // "x200_500Hz_LEFT_Energy",
        // "x500_1000Hz_LEFT_Energy",
        // "x1000_1500Hz_LEFT_Energy",
        // "x1500_2000Hz_LEFT_Energy",
        // "x2000_2500Hz_LEFT_Energy",
        // "x2500_3000Hz_LEFT_Energy",
        "x3000_3500Hz_LEFT_Energy",
        "x3500_4000Hz_LEFT_Energy",
        "x4000_4500Hz_LEFT_Energy",
        "x4500_5000Hz_LEFT_Energy",
    ]
}];
let keysss123=[{
    "key":"LEFT_SPECTRAL_LEFT_CENTROID",
    x:[
        "x3000_3500Hz_LEFT_SPECTRAL_LEFT_CENTROID",
        "x3500_4000Hz_LEFT_SPECTRAL_LEFT_CENTROID",
        "x4000_4500Hz_LEFT_SPECTRAL_LEFT_CENTROID",
        "x4500_5000Hz_LEFT_SPECTRAL_LEFT_CENTROID"
    ]
}]
keysss.map(item=>{
    $(".new-select").append(`<option value="${item.key}">${item.key}</option>`);
});

let ind="LEFT_SPECTRAL_LEFT_CENTROID";
let viztype="spline";
function prChart(data,slectedIndex,singleRecord,type){
   
    if(type!==undefined){
        viztype=type;
    }else{
         type="spline";
    }
    ind="LEFT_SPECTRAL_LEFT_CENTROID";
    if(slectedIndex!==undefined && slectedIndex!=="select" && slectedIndex!==''){
       ind=slectedIndex;
    }
    let cats=keysss.filter(t=>t.key===ind);
    console.log(cats)
    let xcats=[
        // "x200Hz Left "+cats[0].key,
        // "x200_500Hz " +cats[0].key,
        // "x500_1000Hz " +cats[0].key,
        // "x1000_1500Hz " +cats[0].key,
        // "x1500_2000Hz " +cats[0].key,
        // "x2000_2500Hz " +cats[0].key,
        // "x2500_3000Hz " +cats[0].key,
        "x3000_3500Hz " +cats[0].key,
        "x3500_4000Hz " +cats[0].key,
        "x4000_4500Hz " +cats[0].key,
        "x4500_5000Hz "+cats[0].key
    ]
    let g=[];
    data.forEach(i=>{
            cats.forEach(k=>{
                 let arr=[];
              k.x.forEach(l=>{
                  if(i[l]===undefined){
                      console.log(i[l],l,i.Patient_ID,i.BMI);
                  }
                arr.push(parseInt(i[l] || "0"))
              })
               g.push({name:i.Patient_ID,data: arr,status:i.Target_Status,bmi:i.BMI})
            })    
    });
    if(singleRecord!==undefined){
        [singleRecord].forEach(i=>{
            cats.forEach(k=>{
                 let arr=[];
              k.x.forEach(l=>{
                  if(i[l]===undefined){
                      console.log(i[l],l,i.Patient_ID,i.BMI);
                  }
                arr.push(parseInt(i[l] || "0"))
              })
               g.push({name:i.Patient_ID,data: arr,status:i.Predicted_Target_status,bmi:i.BMI,new:true})
            })
    });
    }
    console.log("g",g);
    console.log("g",g.length);
    //return;
    ///alert();
    let allcats=keysss.map(item=>item.key);

    //alert(allcats.length)
    let xx=[];//keysss.map(item=>({categories:item.x}));
    console.log(allcats);
    data=g;
        Highcharts.chart('container', {
            chart: {
                type: viztype,
                height:600,
                parallelCoordinates: true,
                parallelAxes: {
                    lineWidth: 2
                }
            },
            title: {
                text: cats[0].key
            },
            plotOptions: {
                series: {
                    animation: false,
                    marker: {
                        enabled: false,
                        states: {
                            hover: {
                                enabled: false
                            }
                        }
                    },
                    states: {
                        hover: {
                            halo: {
                                size: 0
                            }
                        }
                    },
                    events: {
                        mouseOver: function () {
                            this.group.toFront();
                        }
                    }
                }
            },
            tooltip: {
                pointFormat: '<span style="color:{point.color}">\u25CF</span>' +
                    '{series.name}: <b>{point.formattedValue}</b><br/>'
            },
            xAxis: {
                categories: xcats,
                offset: 10
            },
            yAxis:xx,
            colors: ['rgba(11, 200, 200, 0.1)'],
            series: data.map(function (set, i) {
                return {
                    name: "Patient Id  "+set.name,
                    data: set.data,
                    shadow: false,
                    lineWidth: set.new ? 5 : 1,
                     color: set.new ? "black" : (set.status === "0" ? "red" : "blue"),
                };
            })
        });
    
    
}

let series = [];
let mainData=[];
let moreData=[];
let JSONData1=[];
let currentObject={};
 let xaxis = ["x3000_3500Hz_LEFT_SPECTRAL_LEFT_CENTROID", "x3500_4000Hz_LEFT_SPECTRAL_LEFT_CENTROID", "x4000_4500Hz_LEFT_SPECTRAL_LEFT_CENTROID", "x4500_5000Hz_LEFT_SPECTRAL_LEFT_CENTROID"];
 let id=window.location.search.split("=")[1];
//    $.ajax({
//            url:"http://35.244.60.138/timber/api/index.php",
//            method:"POST",
//           data: JSON.stringify({"action":"GETD"}),
//         //  "content-type": "application/json; charset=utf-8",
//           "cache-control" : "no-cache",
//          "processData": false,
//           success:function (data){
//                console.log(data)
//                alert();
//           }
//       });


// $.getJSON('http://35.244.60.138/timbre-admin/public/assets/data/data.json',function(JSONData){
//     // prChart(JSONData);
//      //return;
//     JSONData1=JSONData;
//     JSONData.forEach(main => {
//         let obj = {
//             name: main.Patient_ID,
//             color: main.Target_Status === "0" ? "red" : "green",
//             data: [],
//              type: 'area',
//         }
//         xaxis.forEach(x => {
//             obj.data.push(parseFloat(main[x]));
//         });
//         series.push(obj);
//     })
    // prepareChart(xaxis, series)
    
    //  if(id){
         //$.getJSON('http://35.244.60.138/timbre-admin/public/assets/data/moredata.json',
         
         $.ajax({
           //url:"http://35.244.60.138/timber/api/index.php",
        //url:"http://localhost/blog/app/New%20folder/timbre-admin/public/assets/data/moredata.json",
           url:"http://35.244.60.138/timbre-admin/public/assets/data/moredata.json",
           method:"GET",
          data: JSON.stringify({"action":"GETD"}),
        //  "content-type": "application/json; charset=utf-8",
          "cache-control" : "no-cache",
         "processData": false,
          success:function(mainData){
             prChart(mainData);
             moreData=mainData;
              let a=mainData.filter(item=>item.Patient_ID===id);
           
                  let main={};
                  if(main){                  
                     $.ajax({
                           url:"http://52.183.8.147/timbre-api/api/index.php",
                           method:"POST",
                          data: JSON.stringify({"action":"GETPID",id:id}),
                        //  "content-type": "application/json; charset=utf-8",
                          "cache-control" : "no-cache",
                         "processData": false,
                          success:function (data){
                              if(data.length===0){
                                $(".loader-container").hide(500);

                                return;       
                              }
                              main=data[0];
                              currentObject=main;
                              prChart(moreData,ind,main);
                              let bmi=main['BMI'];
                              bmi=parseFloat(bmi);
                              if(!isNaN(bmi)){
                                bmi= bmi.toFixed(2);
                                if(bmi < 18){
                                    $('.bmi').css({"font-weight": "900","color":"red"});
                                }
                              }else{
                                bmi=undefined;
                              }
                             bindData('bmi',bmi || main['BMI']);
                             let ss= main['Predicted_Target_status'] === "0" ? "0 (Dry cough)" : "1 (Wet cough)"
                           //console.log(main,"here===========>");
                            bindData('pstatus',ss);
                            bindData('pid',main['Patient_ID']);
                            bindData('gender',main['Gender']);
                            bindData('age',main['Age']);
                            bindData('Cough_Type',main['Cough_Type']);
                            bindData('AppetitePattern',main['AppetitePattern']);
                            if(main['AppetitePattern']==='Low' || main['AppetitePattern']==='low'){
                                    $('.AppetitePattern').css({"font-weight": "900","color":"red"});
                            }
                            bindData('wh',`${main['Weight']} x ${main['Height']}`);
                            bindData('Current_Medications',main['Current_Medications']);
                            let Current_Medications=main['Current_Medications'];
                            if(Current_Medications.includes('tb') || Current_Medications.includes('TB') || Current_Medications.includes('Tb')){
                                $('.Current_Medications').css({"font-weight": "900","color":"red"});
                            }
                            bindData('Occupation',main['Occupation']);
                            bindData('AppetitePattern',main['AppetitePattern']);
                            $(".loader-container").hide(500);
                                                        
                            
                            $(".loader").hide(500);
                            
                          }
                      })
                    
                   
                  }else{
                       alert("we did not find any result with this PID");
                  }
                          
    // });
    //  }else{
    //      alert("We did not find any PID");
    //  }

   ;
}});
function bindData(c,d){
    $(`.${c}`).text(d);
    if(d==""){
        $(`.${c}`).text('-----------');
        $(`.${c}`).css('color','red')
    }
    


}

    function prepareChart(xaxis, series,obj={}) {
        let ddd= [...series, ...[obj]]
        Highcharts.chart('container', {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text:null
                // text: 'Average Monthly Temperature and Rainfall in Tokyo'
            },
            subtitle: {
                // text: 'Source: WorldClimate.com'
            },
            xAxis: [{ // Primary yAxis
                labels: {
                    //format: '{value}��C',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Spectral Centroid',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, {
                categories: xaxis,
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    //format: '{value}��C',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Intensity',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    // text: 'Rainfall',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    enabled: false
                        // format: '{value} mm',
                        // style: {
                        //     color: Highcharts.getOptions().colors[0]
                        // }
                },
                //opposite: true
            }],
            tooltip: {
                enabled: false,
                shared: false
            },
            legend: {
                enabled: false,
                layout: 'vertical',
                align: 'left',
                x: 120,
                verticalAlign: 'top',
                y: 100,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255,255,255,0.25)'
            },
            series:ddd,

            // [{
            //     name: 'All Partients',
            //     type: 'area',
            //     yAxis: 1,
            //     data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
            //     // tooltip: {
            //     //     valueSuffix: ' mm'
            //     // }

            // }, {
            //     name: 'Patient Id : 456 ',
            //     type: 'spline',
            //     data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6],
            //     // tooltip: {
            //     //     valueSuffix: '��C'
            //     // }
            // }]
        });
    }

    // $.getJSON(
    //     'https://cdn.jsdelivr.net/gh/highcharts/highcharts@v7.0.0/samples/data/usdeur.json',
    //     function(data) {

    //         Highcharts.chart('container', {
    //             chart: {
    //                 zoomType: 'x'
    //             },
    //             title: {
    //                 text: 'Patient Report'
    //             },
    //             subtitle: {
    //                 text: document.ontouchstart === undefined ?
    //                     'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
    //             },
    //             xAxis: {
    //                 type: 'datetime',
    //                 title: {
    //                     text: 'Frequency'
    //                 }
    //             },
    //             yAxis: {
    //                 title: {
    //                     text: 'Amplitude'
    //                 }
    //             },
    //             legend: {
    //                 enabled: false
    //             },
    //             plotOptions: {
    //                 area: {
    //                     fillColor: {
    //                         linearGradient: {
    //                             x1: 0,
    //                             y1: 0,
    //                             x2: 0,
    //                             y2: 1
    //                         },
    //                         stops: [
    //                             [0, Highcharts.getOptions().colors[0]],
    //                             [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
    //                         ]
    //                     },
    //                     marker: {
    //                         radius: 2
    //                     },
    //                     lineWidth: 1,
    //                     states: {
    //                         hover: {
    //                             lineWidth: 1
    //                         }
    //                     },
    //                     threshold: null
    //                 }
    //             },

    //             series: [{
    //                 type: 'area',
    //                 data: data
    //             }]
    //         });
    //     }
    // );
   
    $("select.new-select").change(function() {
        //console.log(currentObject);
        series = [];
        let status = $(this).val();


       prChart(moreData,status,currentObject)
        return;
        JSONData1.forEach(main => {
            if (main.Target_Status === status) {
                let obj = {
                    name: main.Patient_ID,
                    color: main.Target_Status === "0" ? "red" : "green",
                    data: [],
                    type: 'area',
                }
                xaxis.forEach(x => {
                    obj.data.push(parseFloat(main[x]));
                });
                series.push(obj);
            }
        });
        prepareChart(xaxis, series,currentObject);
    })
        $(".viz-select").change(function() {
      
        series = [];
        let status = $(this).val();
       // alert(status)
       prChart(moreData,$("select.new-select").val(),currentObject,status)
    })
    
   
    console.log(series);
</script>

        </body>
    </html>