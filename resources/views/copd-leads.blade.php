    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
            <title>Timbre admin</title>
            <link rel="icon"  type="icon/css" href="{{ asset('images/appicon.png') }}">

            <!-- Bootstrap -->
            <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
            <!--side menu plugin-->
            <link href="{{ asset('plugins/hoe-nav/hoe.css ') }}" rel="stylesheet">
            <!-- icons-->
            <link href="{{ asset('plugins/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
            <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
            <link href="{{ asset('plugins/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet" />
            <link href="{{ asset('plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
            <!--template custom css file-->
            <link href="{{ asset('css/style.css')}}" rel="stylesheet">

            <script src="{{ asset('js/modernizr.js')}}"></script>
            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
            <style>
        .table{
            font-size:12px;
        }
            </style>
        </head>
        <body hoe-navigation-type="vertical" hoe-nav-placement="left" theme-layout="wide-layout">

            <!--side navigation start-->
            <div id="hoeapp-wrapper" class="hoe-hide-lpanel" hoe-device-type="desktop">
                @include('includes.header')
                <div id="hoeapp-container" hoe-color-type="lpanel-bg7" hoe-lpanel-effect="shrink">
                    
                @include('includes.sidemenu')

                    <!--start main content-->
                    <section id="main-content">
                        <div class="space-30"></div>
                        <!-- @yeld('content') -->
                        <div class="container">
                            <!--widget box row-->
                            <div class="row">
                               
                                <div class="col-md-12">
                                    <div class="panel table-top">
                                        <header class="panel-heading" style="display: table; width: 100%;">
                                            <h2 style="float: left;    line-height: 45px;" class="panel-title">All Leads</h2>
                                           
                                        </header>
                                        <div class="panel-body">
                                         <a class="pull-right don-btn" href="{{url('download-data')}}"> Download Data </a>
                                          <div class="table-responsive">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Patient ID</th>
                                                <th>Patient Name</th>
                                                <th>Organization</th>
                                                <th>Gender</th>
                                                <th>Age</th>
                                                <!-- <th>Image</th> -->
                                                <th>MPA-Audio</th>
                                                <!-- <th>Mobile-Audio</th> -->
                                                <!-- <th>Mode(Offline/Online)</th> -->
                                                <th>Created On</th>
                                                <th>Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
function convertedDate($date){
    $date = new DateTime($date, new DateTimeZone('GMT'));
    $date->setTimezone(new DateTimeZone('IST'));
    return $date->format('Y-m-d H:i:s');
}
                                        ?>
                                       @foreach ($dbresult as $i=>$row)
                                            <tr>
                                                <th scope="row">{{$i++}}</th>
                                                <td>{{$row->record_id}}</td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$row->organization}}</td>
                                                <td>{{$row->gender}}</td>
                                                <td>{{$row->age}}</td>
                                             
                                                <td>
                                                @if($row->audio_path=="null")
                                                    <a href="JavaScript:Void(0);" target="_blank">No File</a>
                                                @else
                                                    <a href="http://52.183.8.147/timbre-api/api/uploads/{{$row->audio_path}}" target="_blank">Click here</a>
                                                @endif
                                                </td>
                                                <td>{{convertedDate($row->created_on)}}</td>
                                                <td>
                                                    <div class="">
                                                      <a class="btn btn-xs btn-default viewDetails" data-id="{{$row->record_id}}" ><i class="ion-eye"></i> View</a>
                                                      <a  onclick='return confirmDelete()' class="btn btn-xs btn-danger" href="{{ url('/deleteCopdLead', ['id' => $row->record_id]) }}">
                                                        <i class="ion-trash-a"></i> Delete</a>
                                                      <!-- <a class="btn btn-xs btn-success"><i class="ion-ios-download-outline"></i> Download</a> -->
                                                    </div>
                                                </td>
                                            </tr>
                                          
                                            @endforeach
                                            
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12">
                                            
                                            <div class="center">
                                                {{ $dbresult->links() }}
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div><!--col end-->
                            </div><!--row end-->
                        </div><!--end container-->

                        <!--footer start-->
                        <div class="footer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <span>&copy; Copyright 2016. Timber</span>
                                </div>
                            </div>
                        </div>
                        <!--footer end-->
                    </section><!--end main content-->
                </div>
            </div><!--end wrapper-->


            <div id="myModal" class="modal fade myModal" role="dialog">
              <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">More Details</h4>
                  </div>
                  <div class="modal-body" id="modalBody">
                
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>

            <!--Common plugins-->
            <script src="{{ asset('plugins/jquery/dist/jquery.min.js')}}"></script>
            <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
            <!-- <script src="{{ asset('plugins/hoe-nav/hoe.js')}}"></script>
            <script src="{{ asset('plugins/pace/pace.min.js')}}"></script>
            <script src="{{ asset('plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
            <script src="{{ asset('js/app1.js')}}"></script> -->
            <!--page scripts-->
            <!-- Flot chart js -->
            <!-- <script src="{{ asset('plugins/flot/jquery.flot.js')}}"></script>
            <script src="{{ asset('plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
            <script src="{{ asset('plugins/flot/jquery.flot.resize.js')}}"></script>
            <script src="{{ asset('plugins/flot/jquery.flot.pie.js')}}"></script>
            <script src="{{ asset('plugins/flot/jquery.flot.time.js')}}"></script> -->
            <!--vector map-->
            <!-- <script src="{{ asset('plugins/vectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
            <script src="{{ asset('plugins/vectormap/jquery-jvectormap-world-mill-en.js')}}"></script> -->
            <!-- ChartJS-->
            <!-- <script src="{{ asset('plugins/chartJs/Chart.min.js')}}"></script> -->
            <!--dashboard custom script-->
            <!-- <script src="{{ asset('js/dashboard.js')}}"></script> -->

            <script type="text/javascript">
            
                function confirmDelete() {
                    return confirm('Are you sure you want to delete?');
                }
                $( document ).ready(function() {
                    function modelListComponent(id,columns,data){
                        $(`#${id}`).empty();
                        let lis=columns.map(c=>`<li>
                        <strong>${c.title}</strong>
                        <p>${data[c.key] || "----"}</p>
                        </li>`).join('');
                        let htmlStr=`<ul class="modal-list-items">${lis}</ul>`;
                        $(`#${id}`).append(htmlStr)
                     }
                let leadsColumns=[{"title":"record_id","key":"record_id"},{"title":"name","key":"name"},{"title":"current_location","key":"current_location"},{"title":"gender","key":"gender"},{"title":"age","key":"age"},{"title":"height_feet","key":"height_feet"},{"title":"height_inches","key":"height_inches"},{"title":"marital_status","key":"marital_status"},{"title":"weight","key":"weight"},{"title":"organization","key":"organization"},{"title":"occupation","key":"occupation"},{"title":"audio_path","key":"audio_path"},{"title":"created_on","key":"created_on"},{"title":"created_by","key":"created_by"} ,{"title":"chest_tightness","key":"chest_tightness"},{"title":"wheezing","key":"wheezing"},{"title":"updated_on","key":"updated_on"},{"title":"updated_by","key":"updated_by"},{"title":"id","key":"id"},{"title":"smoking","key":"smoking"},{"title":"sob","key":"sob"},{"title":"sputum","key":"sputum"},{"title":"lung_conditions","key":"lung_conditions"},{"title":"excercise","key":"excercise"}];
                $(".viewDetails").click(function(){ 
                    $.get(`copd-getAllParams/${$(this).attr('data-id')}`,function(res){
                        modelListComponent('modalBody',leadsColumns,(res[0] || {}));
                        $("#myModal").modal('show');
                    })
                })

                })
               
                </script>
        </body>
    </html>