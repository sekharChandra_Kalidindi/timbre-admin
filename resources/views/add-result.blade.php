    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
            <title>Timbre admin</title>
            <link rel="icon"  type="icon/css" href="{{ asset('images/appicon.png') }}">

            <!-- Bootstrap -->
            <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
            <!--side menu plugin-->
            <link href="{{ asset('plugins/hoe-nav/hoe.css ') }}" rel="stylesheet">
            <!-- icons-->
            <link href="{{ asset('plugins/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
            <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
            <link href="{{ asset('plugins/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet" />
            <link href="{{ asset('plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
            <!--template custom css file-->
            <link href="{{ asset('css/style.css')}}" rel="stylesheet">
            <link href="{{ asset('css/w3.css')}}" rel="stylesheet">

            <script src="{{ asset('js/modernizr.js')}}"></script>
            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
        </head>
        <body hoe-navigation-type="vertical" hoe-nav-placement="left" theme-layout="wide-layout">

            <!--side navigation start-->
            <div id="hoeapp-wrapper" class="hoe-hide-lpanel" hoe-device-type="desktop">
                @include('includes.header')
                <div id="hoeapp-container" hoe-color-type="lpanel-bg7" hoe-lpanel-effect="shrink">
                    
                @include('includes.sidemenu')

                    <!--start main content-->
                    <section id="main-content">
                        <div class="space-30"></div>
                        <!-- @yeld('content') -->
                        <div class="container">
                            
                            <div class="row">
                               
                                <div class="col-md-12">
                                    <div class="panel table-top">
                                        <header class="panel-heading">
                                            <h2 class="panel-title" style="color:#fff">Add Result </h2>
                                        </header>
                                        <div class="panel-body">
                                            <div class="w3-row">

          
            <div class="w3-col l6">
                <!-- Posts -->
                <div class="w3-card w3-margin">
                   
                    <ul class="w3-ul w3-hoverable w3-white">
                        <li class="w3-padding-16">
                            <span class="w3-large">Patient Id</span> 
                           <input type="text" class="form-control"/>
                        </li>
                       <li class="w3-padding-16">
                            <span class="w3-large">Patient Name</span> 
                           <input type="text" class="form-control"/>
                        </li>
                        <li class="w3-padding-16">
                            <span class="w3-large">Status</span>
                            <input type="text" class="form-control"/>
                        </li>
                        <li class="w3-padding-16 text-center">
                           <Button type="submit" class="btn btn-success">Submit</Button>
                        </li>
                        
                    </ul>
                </div>
                
                <!-- END Introduction Menu -->
            </div>
             <div class="w3-col l6">


                <!-- Posts -->
                <div class="w3-card w3-margin">
                   
                    <ul class="w3-ul w3-hoverable w3-white">
                        <li class="w3-padding-16">
                            <span class="w3-large">Bulk Upload</span> 
                            <span>Upload the Excelfile</span>
                           <input type="file" class="form-control"/>
                        </li>
                          <li class="w3-padding-16 text-center">
                           <Button type="submit" class="btn btn-success">Submit</Button>
                        </li>
                       
                    </ul>
                </div>
                
                <!-- END Introduction Menu -->
            </div>


            <!-- END GRID -->
        </div><br>

                                           </div>

                                        <div class="col-md-12">
                                            
                                            <div class="center">
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div><!--col end-->
                            </div>


                        </div><!--end container-->

                        <!--footer start-->
                        <div class="footer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <span>&copy; Copyright 2016. Timber</span>
                                </div>
                            </div>
                        </div>
                        <!--footer end-->
                    </section><!--end main content-->
                </div>
            </div><!--end wrapper-->

            <!--Common plugins-->
            <script src="{{ asset('plugins/jquery/dist/jquery.min.js')}}"></script>
            <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
            <script src="{{ asset('plugins/hoe-nav/hoe.js')}}"></script>
            <script src="{{ asset('plugins/pace/pace.min.js')}}"></script>
            <script src="{{ asset('plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
            <script src="{{ asset('js/app1.js')}}"></script>
            <!--page scripts-->
            <!-- Flot chart js -->
            <script src="{{ asset('plugins/flot/jquery.flot.js')}}"></script>
            <script src="{{ asset('plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
            <script src="{{ asset('plugins/flot/jquery.flot.resize.js')}}"></script>
            <script src="{{ asset('plugins/flot/jquery.flot.pie.js')}}"></script>
            <script src="{{ asset('plugins/flot/jquery.flot.time.js')}}"></script>
            <!--vector map-->
            <script src="{{ asset('plugins/vectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
            <script src="{{ asset('plugins/vectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
            <!-- ChartJS-->
            <script src="{{ asset('plugins/chartJs/Chart.min.js')}}"></script>
            <!--dashboard custom script-->
            <script src="{{ asset('js/dashboard.js')}}"></script>
          

        </body>
    </html>