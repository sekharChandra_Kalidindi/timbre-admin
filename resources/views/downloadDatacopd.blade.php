<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Timbre admin</title>
      <link rel="icon"  type="icon/css" href="{{ asset('images/appicon.png') }}">
      <!-- Bootstrap -->
      <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
      <!--side menu plugin-->
      <link href="{{ asset('plugins/hoe-nav/hoe.css ') }}" rel="stylesheet">
      <!-- icons-->
      <link href="{{ asset('plugins/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
      <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
      <link href="{{ asset('plugins/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet" />
      <link href="{{ asset('plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
      <!--template custom css file-->
      <link href="{{ asset('css/style.css')}}" rel="stylesheet">
      <script src="{{ asset('js/modernizr.js')}}"></script>
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body hoe-navigation-type="vertical" hoe-nav-placement="left" theme-layout="wide-layout">
      <!--side navigation start-->
      <div id="hoeapp-wrapper" class="hoe-hide-lpanel" hoe-device-type="desktop">
         @include('includes.header')
         <div id="hoeapp-container" hoe-color-type="lpanel-bg7" hoe-lpanel-effect="shrink">
            @include('includes.sidemenu')
            <!--start main content-->
            <section id="main-content">
               <div class="space-30"></div>
               <!-- @yeld('content') -->
               <div class="container">
                  <!--widget box row-->
                  <div class="col-md-12">
                     <div class="panel table-top">
                        <header class="panel-heading" style="display: table; width: 100%;">
                           <h2 style="float: left;    line-height: 35px;" class="panel-title">All COPD Leads</h2>
                           <!-- <a class="pull-right don-btn" href="#"> Download Data </a> -->
                        </header>
                        <div style="padding-top:20px;"> 
                           <div class="col-md-9">
                              <div class="row">
                                 <div class="col-md-12">
                                    <form method="post" action="{{url('/copd-filter')}}">
                                       <input type="hidden" value="search" name="filter_type">
                                       <div class="row">
                                          <div class="col-md-2">
                                          <strong>Patient ID</strong>
                                          <input id="searc_id__box"  value="{{$searchid}}"  type="text"  name="src_id_string" placeholder="Search by patient ID" class="form-control namesearch">
                                             </div>
                                          <div class="col-md-2">
                                          <strong>Patient Name</strong>
                                             <input id="searc_box"  value="{{$searchdata}}"  type="text"  name="src_string" placeholder="Search by patient name" class="form-control namesearch">
                                          </div>
                                          <div class="col-md-2">
                                          <strong>Org Name</strong>
                                             <select  class="form-control org margin-b-20" name="org">
                                                <option value="">select organization</option>
                                                @foreach ($dbresult2 as $i=>$row)
                                                 @if($org==$row->organization)
                                                  <option selected>{{$row->organization}}</option>
                                                @else
                                                <option >{{$row->organization}}</option>
                                                @endif
                                                @endforeach
                                             </select>
                                          </div>
                                          <div class="col-md-4 ">
                                          <strong>Date Range</strong>
                                          <div>
                                             <input id="fromDate" value="{{$fromDate}}" name="fromDate" style="width:50%;float:left" type="date" placeholder="Start Date" class="form-control fromdate ">
                                             <input id="toDate" value="{{$toDate}}"  name="toDate" type="date" style="width:50%;float:left" placeholder="End Date" class="form-control todate">
                                          </div>
                                          </div>
                                          <div class="col-md-1">
                                          <strong>-</strong>
                                             <button id="searc_Btn" type="submit" class="btn btn-primary">Search
                                             </button>
                                          </div>
                                       </div>
                                       <div class="row" style="margin:0px;">
                                          <!-- <span class="input-group-btn">
                                             <button id="searc_Btn" type="submit" class="btn btn-primary">Search
                                             </button>
                                             </span> -->
                                       </div>
                                    </form>
                                 </div>
                                 <!-- <div class="col-md-6">
                                    <div class="form-group">
                                    <form method="post" action="{{url('/filter')}}">
                                       <input type="hidden" value="cat" name="filter_type">
                                         <div class="input-group margin-b-20">
                                             <div class="input-group-btn123" >
                                                 
                                                 <select style="width:150px" class="form-control margin-b-20" name="org">
                                                     <option value="">select organization</option>
                                                      @foreach ($dbresult2 as $i=>$row)
                                                      <option>{{$row->organization}}</option>
                                                      @endforeach
                                                 </select>
                                                 <button type="submit" class="btn btn-primary">Search
                                               </button>
                                    
                                             </div>
                                             <input type="text" placeholder="Search"  name="src_string" class="form-control hide">
                                              <span class="input-group-btn">
                                              
                                               </span>
                                         </div>
                                         </form>
                                     </div>
                                    </div> -->
                                 <!-- <div class="col-md-6">
                                    <div class="form-group">
                                         <div class="input-group margin-b-20" style="    width: 112%;">
                                         <form method="post" action="{{url('/filter')}}">
                                       <input type="hidden" value="date" name="filter_type">
                                              <input id="fromDate" name="fromDate" style="width:40%;float:left" type="date" placeholder="Start Date" class="form-control">
                                             <input id="toDate" name="toDate" type="date" style="width:40%;float:left" placeholder="End Date" class="form-control">
                                              <span class="input-group-btn" style="width:10%;display:block;">
                                               <button id="dateBtn" type="submit" class="btn btn-primary">Search
                                               </button>
                                               </span>
                                               </form>
                                         </div>
                                     </div>
                                    </div> -->
                              </div>
                           </div>
                           <div class="col-md-3">
                              <div class="">
                                 <div class="col-md-12 ">
                                    <div class="form-group pull-right">
                                    <strong>Download</strong>
                                       <div class="input-group margin-b-20">
                                          <select style="width:150px" id="selectDataType" class="form-control margin-b-20" name="account">
                                             <option selected="">Select Data Type</option>
                                             <option  value="show_Data_popup">Data</option>
                                             <option id="show_Images_popup" value="show_Images_popup">Images </option>
                                             <option id="show_Voice_popup" value="show_Voice_popup">Voice</option>
                                          </select>
                                          <span class="input-group-btn input-group-btn1">
                                          <button type="button" id="download_data" class="btn btn-primary">Download
                                          </button>
                                          <a id="sss" href="download">sss</a>
                                          </span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="panel-body">
                           <div class="table-responsive">
                              <table class="table table-hover table-bordered">
                                 <thead>
                                    <tr>
                                       <th> <input type="checkbox" /> </th>
                                       <th>Patient ID</th>
                                       <th>Patient Name</th>
                                       <th>Organization</th>
                                       <th>Gender</th>
                                       <th>Age</th>
                                       <th>Created On</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach ($dbresult as $i=>$row)
                                    <tr>
                                       <th scope="row"> <input type="checkbox" /></th>
                                       <td>{{$row->record_id}}</td>
                                       <td>{{$row->name}}</td>
                                       <td>{{$row->organization}}</td>
                                       <td>{{$row->gender}}</td>
                                       <td>{{$row->age}}</td>
                                       <td>{{$row->created_on}}</td>
                                       <td>
                                          <div class="">
                                             <a class="btn btn-xs btn-default viewDetails" data-id="{{$row->record_id}}"><i class="ion-eye"></i> View</a>
                                             <a class="btn btn-xs btn-danger"><i class="ion-trash-a"></i> Delete</a>
                                             <!-- <a class="btn btn-xs btn-success"><i class="ion-ios-download-outline"></i> Download</a> -->
                                          </div>
                                       </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                           <div class="col-md-12">
                              <div class="center">
                                 @if($dbresult instanceof  \Illuminate\Pagination\LengthAwarePaginator)
                                 {{ $dbresult->links() }}
                                 @endif
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!--col end-->
               </div>
               <!--row end-->
         </div>
         <!--end container-->
         <!--footer start-->
         <div class="footer">
         <div class="row">
         <div class="col-sm-12">
         <span>&copy; Copyright 2016. Timber</span>
         </div>
         </div>
         </div>
         <!--footer end-->
         </section><!--end main content-->
      </div>
      </div><!--end wrapper-->
      <div id="show_Data_popup" class="modal fade" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Modal Header imaes</h4>
               </div>
               <div class="modal-body" style="display:table;width:100%">
                  <div class="row slectedRows">
                  </div>
               </div>
               <div class="modal-footer">
                  <button type="submit" id="popupSubmit_btm" class="btn btn-success">Download</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>


      <div id="myModal" class="modal fade myModal" role="dialog">
              <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">More Details</h4>
                  </div>
                  <div class="modal-body" id="modalBody">
                
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>
      <!--Common plugins-->
      <script src="{{ asset('plugins/jquery/dist/jquery.min.js')}}"></script>
      <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
      <script src="{{ asset('plugins/hoe-nav/hoe.js')}}"></script>
      <script src="{{ asset('plugins/pace/pace.min.js')}}"></script>
      <script src="{{ asset('plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
      <script src="{{ asset('js/app1.js')}}"></script>
      <!--page scripts-->
      <!-- Flot chart js -->
      <script src="{{ asset('plugins/flot/jquery.flot.js')}}"></script>
      <script src="{{ asset('plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
      <script src="{{ asset('plugins/flot/jquery.flot.resize.js')}}"></script>
      <script src="{{ asset('plugins/flot/jquery.flot.pie.js')}}"></script>
      <script src="{{ asset('plugins/flot/jquery.flot.time.js')}}"></script>
      <!--vector map-->
      <script src="{{ asset('plugins/vectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
      <script src="{{ asset('plugins/vectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
      <!-- ChartJS-->
      <script src="{{ asset('plugins/chartJs/Chart.min.js')}}"></script>
      <!--dashboard custom script-->
      <script src="{{ asset('js/dashboard.js')}}"></script>
      <script type="text/javascript">
         let cols=["record_id","name","id","current_location","gender","height_feet","age","height_feet","height_inches","marital_status","weight","organization","smoking","occupation","audio_path","created_on","created_by","updated_on","updated_by","sob","sputum","lung_conditions","excercise", "chest_tightness","wheezing"]; 
         
         $(".slectedRows").empty();
         let html="";
         cols.forEach(item=>{
              html=html+`<div class="" style="background:#f1f1f1;width:32%;margin:0.5%;float:left;height:25px;margin-bottom:0px">
                         <div class="i-checks">
                             <label> 
                                 <input class="data_fields" type="checkbox" data-item-value="${item}" value="${item}">
                                  <i></i>
                                   ${item}
                             </label>
                         </div>
                     </div>`;
         })
          html=html+`<div style="clear:both"></div>`;
         $(".slectedRows").append(html);
         
         // $("#download_data").click(function(){
         //     var downloadLink;
         //     downloadLink = document.createElement("a");
         //     document.body.appendChild(downloadLink);
         //     downloadLink.href ='http://localhost/blog/app/New%20folder/timbre-admin/public/download?name=v'; //'data:' + dataType + ', ' + tableHTML;
         //     downloadLink.click();
         // })
             $("#selectDataType").change(function () {
                 var end = this.value;
                 if(end==="show_Data_popup"){
                     $('#show_Data_popup').modal();
                 }else{
                  downloadMedia(this.value);
                 }
             });

        //     $( document ).ready(function() {
                    function modelListComponent(id,columns,data){
                        $(`#${id}`).empty();
                        let lis=columns.map(c=>`<li>
                        <strong>${c.title}</strong>
                        <p>${data[c.key] || "----"}</p>
                        </li>`).join('');
                        let htmlStr=`<ul class="modal-list-items">${lis}</ul>`;
                        $(`#${id}`).append(htmlStr)
                     }
                //let leadsColumns=[{"title":"record_id","key":"record_id"},{"title":"aadhar","key":"aadhar"},{"title":"firstname","key":"firstname"},{"title":"lastname","key":"lastname"},{"title":"mobile","key":"mobile"},{"title":"gender","key":"gender"},{"title":"age","key":"age"},{"title":"height_feet","key":"height_feet"},{"title":"height_inches","key":"height_inches"},{"title":"marital_status","key":"marital_status"},{"title":"weight","key":"weight"},{"title":"organization","key":"organization"},{"title":"occupation","key":"occupation"},{"title":"other_occupation","key":"other_occupation"},{"title":"prescription_img","key":"prescription_img"},{"title":"audio_path","key":"audio_path"},{"title":"created_on","key":"created_on"},{"title":"created_by","key":"created_by"},{"title":"updated_on","key":"updated_on"},{"title":"updated_by","key":"updated_by"},{"title":"id","key":"id"},{"title":"smoking","key":"smoking"},{"title":"alcohol","key":"alcohol"},{"title":"tb_family_history","key":"tb_family_history"},{"title":"living_with_tb","key":"living_with_tb"},{"title":"paroxysmal_cough","key":"paroxysmal_cough"},{"title":"hiv","key":"hiv"},{"title":"diabetes","key":"diabetes"},{"title":"night_sweat","key":"night_sweat"},{"title":"loss_of_appetite","key":"loss_of_appetite"},{"title":"exercise","key":"exercise"},{"title":"supplement","key":"supplement"},{"title":"chestpain","key":"chestpain"},{"title":"current_fever_pattern","key":"current_fever_pattern"},{"title":"appetite_pattern","key":"appetite_pattern"},{"title":"cough_frequency","key":"cough_frequency"},{"title":"cough_type","key":"cough_type"},{"title":"travelling_frequency","key":"travelling_frequency"},{"title":"current_medication","key":"current_medication"},{"title":"other_cough","key":"other_cough"},{"title":"visit","key":"visit"},{"title":"total_qty","key":"total_qty"},{"title":"total_consumed","key":"total_consumed"},{"title":"breaths","key":"breaths"}];
                let leadsColumns=[{"title":"record_id","key":"record_id"},{"title":"name","key":"name"},{"title":"current_location","key":"current_location"},{"title":"gender","key":"gender"},{"title":"age","key":"age"},{"title":"height_feet","key":"height_feet"},{"title":"height_inches","key":"height_inches"},{"title":"marital_status","key":"marital_status"},{"title":"weight","key":"weight"},{"title":"organization","key":"organization"},{"title":"occupation","key":"occupation"},{"title":"audio_path","key":"audio_path"},{"title":"created_on","key":"created_on"},{"title":"created_by","key":"created_by"},{"title":"updated_on","key":"updated_on"},{"title":"updated_by","key":"updated_by"},{"title":"id","key":"id"},{"title":"smoking","key":"smoking"},{"title":"sob","key":"sob"},{"title":"sputum","key":"sputum"} ,{"title":"chest_tightness","key":"chest_tightness"},{"title":"wheezing","key":"wheezing"},{"title":"lung_conditions","key":"lung_conditions"},{"title":"excercise","key":"excercise"}];

                $(".viewDetails").click(function(){
                   //alert($(this).attr('data-id'));
                    $.get(`get-allcopd-params/${$(this).attr('data-id')}`,function(res){
                       //console.log(res);
                        modelListComponent('modalBody',leadsColumns,(res[0] || {}));
                        $("#myModal").modal('show');
                    })
                })

               // })
               
      </script>
      <!-- Main Searc box find -->
      <script type="text/javascript">
         $("#searc_Btn").click(function() {
                 var textboxvalue = $('#searc_box').val();
                 $.ajax({
                     type: "POST",
                     url: 'ajaxPage.php',
                     data: {txt1: textboxvalue},
                     success: function(result) {
                         $("div").html(result);
                     }
                 });
             });
      </script>
      <!-- Popup Ceck box find -->
      <script type="text/javascript">
         var i = 0;
             $("#popupSubmit_btm").click(function() {
                     var arr = [];
                     $('.data_fields').each(function(){       
                         if($(this).is(":checked")){
                             arr.push($(this).val());
                         } 
                        
                     });
                     let namesearch=$('.namesearch').val();
                     let org=$('.org').val();
                     let fromdate=$('.fromdate').val();
                     let todate=$('.todate').val();
                     let q=`&search=${namesearch}&org=${org}&fromdate=${fromdate}&todate=${todate}`;
                    // console.log(q);

                     if(arr.length!==0){
                          var downloadLink;
                     downloadLink = document.createElement("a");
                     document.body.appendChild(downloadLink);
                     // /http://localhost/blog/app/New%20folder/timbre-admin/public/
                     downloadLink.href ='http://35.244.60.138/timbre-admin/public/download-data-copd?type=single&selection='+arr.join(',')+q; //'data:' + dataType + ', ' + tableHTML;
                     downloadLink.click();
                     }
                    
         
                    // var a = $('.data_fields:checked').map(function(_, el) {
                    //      return $(el).val();
                    //  }).get();
                 });

                 function downloadMedia(type) {
                var typeTxt="Audio";
                if(type==="show_Images_popup"){
                  typeTxt="images";
                }
                 
                     let namesearch=$('.namesearch').val();
                     let org=$('.org').val();
                     let fromdate=$('.fromdate').val();
                     let todate=$('.todate').val();
                     let q=`&search=${namesearch}&org=${org}&fromdate=${fromdate}&todate=${todate}`;
                    
                     var downloadLink;
                     downloadLink = document.createElement("a");
                     document.body.appendChild(downloadLink);
                     downloadLink.href ='download-media?type='+typeTxt+'&'+q; //'data:' + dataType + ', ' + tableHTML;
                     
                     //alert(downloadLink.href)
                     downloadLink.click();                    
         
                 };
      </script>
      <script type="text/javascript">
         $("#dateBtn").click(function() {
            var fromdata = $("#fromDate").val();
            var todata = $("#toDate").val();
         });
         
      </script>
   </body>
</html>