<div class="table-responsive">
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                @foreach ($columns as $value)
                <th>{{$value['dispalayName']}}</th>
                 @endforeach

            </tr>
        </thead>
        <tbody>
        
        @foreach ($rows as $value) 
            <tr>
            @foreach ($columns as $col)
               @if (array_key_exists('view_type', $col)) 
                <td>
                 <a href="<?php echo $col['rout'];?>"> 
                  <?php echo $value->{$col['accessKey']} ?>
                 </a>
                </td>
                @else 
               <td> <?php echo $value->{$col['accessKey']} ?></td>
                @endif
             @endforeach
            </tr>
       @endforeach
           
        </tbody>
    </table>
</div>