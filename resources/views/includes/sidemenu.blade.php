<aside id="hoe-left-panel" hoe-position-type="absolute">
    <ul class="nav panel-list">

      @if($user_type=='1' || $user_type=='2' || $user_type=='3')
        <li class="{{ Request::segment(1) === 'index' ? 'active' : null }}">
            <a href="{{url('index')}}">
                <i class="fa fa-home"></i>
                <span class="menu-text">Dashboard</span>
                <span class="selected"></span>
            </a>
        </li>
        @endif

        @if(($user_type=='1'  || $user_type=='2' || $user_type=='3') &&  $user_org != "abbott")

        <li class="{{ Request::segment(1) === 'result' ? 'active' : null }}">
            <a href="{{url('result')}}">
               <i class="fa fa-database" aria-hidden="true"></i>
                <span class="menu-text">Timbre Result</span>
                <span class="selected"></span>
            </a>
        </li>
        @endif


        <!-- <li class="{{ Request::segment(1) === 'reporats' ? 'active' : null }}">
            <a href="{{url('reporats')}}">
                <i class="fa fa-user"></i>
                <span class="menu-text">Reporats</span>
                <span class="selected"></span>
            </a>
        </li> -->
        @if($user_type=='1'  || $user_type=='2')
        <li class="{{ Request::segment(1) === 'leads' ? 'active' : null }}">
            <a href="{{url('leads')}}">
                <i class="fa fa-cubes" aria-hidden="true"></i>
                <span class="menu-text">TimBre Leads</span>
                <span class="selected"></span>
            </a>
        </li>
        @endif

        @if($user_type=='1'  || $user_type=='2' )
        <li class="{{ Request::segment(1) === 'download-data' ? 'active' : null }}">
            <a href="{{url('download-data')}}">
                <i class="fa fa-download"></i>
                <span class="menu-text">TimBre Download</span>
                <span class="selected"></span>
            </a>
        </li>
        @endif



        @if(($user_type=='1'  || $user_type=='2') || ($user_org=="abbott" &&  $user_type=='3'))

        <li class="{{ Request::segment(1) === 'result' ? 'active' : null }}">
            <a href="{{url('copd-result')}}">
            <i class="fa fa-database" aria-hidden="true"></i>
                <span class="menu-text">COPD Result</span>
                <span class="selected"></span>
            </a>
        </li>
        @endif

        @if(($user_type=='1'  || $user_type=='2') || $user_type=='3' && $user_org=="abbott")
        <li class="{{ Request::segment(1) === 'copd-leads' ? 'active' : null }}">
            <a href="{{url('copd-leads')}}">
                <i class="fa fa-cubes" aria-hidden="true"></i>
                <span class="menu-text">COPD Leads</span>
                <span class="selected"></span>
            </a>
        </li>
        @endif

        
        @if(($user_type=='1'  || $user_type=='2'))
        <li class="{{ Request::segment(1) === 'copd-data' ? 'active' : null }}">
            <a href="{{url('copd-data')}}">
                <i class="fa fa-cubes" aria-hidden="true"></i>
                <span class="menu-text">COPD Download</span>
                <span class="selected"></span>
            </a>
        </li>
        @endif

 @if($user_type=='1' )
        <li class="{{ Request::segment(1) === 'allusers' ? 'active' : null }}">
            <a href="{{url('allusers')}}">
                <i class="fa fa-user"></i>
                <span class="menu-text">User</span>
                <span class="selected"></span>
            </a>
        </li>
        @endif

        
        @if($user_type=='1') 
        <li class="hoe-has-menu {{ (Request::segment(1) === 'accupation' || Request::segment(1) ==='supplyment'|| Request::segment(1) ==='othercough') ? 'opened' : null }}">
            <a href="javascript:void(0)">
                <i class="fa fa-cogs"></i>
                <span class="menu-text">Master Data</span>
                <span class="selected"></span>
            </a>
            <ul class="hoe-sub-menu">
                <li>
                    <a href="{{url('accupation')}}">
                        <span class="menu-text">Accupation</span>
                        <span class="selected"></span>
                    </a>
                </li>
                <li>
                    <a href="{{url('supplyment')}}">
                        <span class="menu-text">Supplyment</span>
                        <span class="selected"></span>
                    </a>
                </li>
                <li>
                    <a href="{{url('othercough')}}">
                        <span class="menu-text">Othercough</span>
                        <span class="selected"></span>
                    </a>
                </li>
                <li>
                    <a href="{{url('organation-list')}}">
                        <span class="menu-text">Organation</span>
                        <span class="selected"></span>
                    </a>
                </li>
                
            </ul>
        </li>
        @endif
       
    </ul>
</aside><!--aside left menu end-->