<header id="hoe-header" hoe-lpanel-effect="shrink">
                    <div class="hoe-left-header">
                        <a href="{{url('index')}}"> <img src="{{ asset('images/logo.png')}} " class="img-responsive"></a>
                        <span class="hoe-sidebar-toggle"><a href="#"></a></span>
                    </div>

                    <div class="hoe-right-header" hoe-position-type="relative">
                        <span class="hoe-sidebar-toggle"><a href="#"></a></span>
                        
                        <ul class="right-navbar navbar-right">
                            <li class="dropdown">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle hide"> <i class="ion-ios-bell-outline"></i> <span class="label label-danger">4</span></a>
                                <ul class="dropdown-menu dropdown-menu-scale lg-dropdown notifications">
                                    <li> <p>You have 3 new notifications <a href="#"> Mark all Read</a></p></li>
                                    <li class="unread-notifications">
                                        <a href="#">
                                            <i class="ion-ios-email-outline pull-right"></i>
                                            <span class="line">You have 8 Messages</span>
                                            <span class="small-line">3 Minutes ago</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="ion-social-twitter-outline pull-right"></i>
                                            <span class="line">You have 3 new followers</span>
                                            <span class="small-line">8 Minutes ago</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="ion-ios-download-outline pull-right"></i>
                                            <span class="line">Download Complete</span>
                                            <span class="small-line">6 Minutes ago</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle"><img src="{{ asset('images/avtar.png')}}" alt="" width="30" class="img-circle"> 
                                
                                  @isset($uname)
                                  <span>  {{$uname}} </span>
                                  @endisset
                                </a>
                                <ul class="dropdown-menu dropdown-menu-scale user-dropdown">
                                    <li><a href="#"><i class="ion-person"></i> Profile </a></li>
                                    <li><a href="{{url('logout')}}"><i class="ion-log-out"></i> Logout </a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </header>